import { Component, OnInit } from '@angular/core';
import { Servicios } from 'src/servicios/servicios';
import { Router } from '@angular/router';
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  DatLog: any = {"Ema":"","Pas":""};
  RecCon: string = "";

  MosPas: string = "No";

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
    public googlePlus: GooglePlus,
    public fb: Facebook
  ) { }

  ngOnInit() {
  }

  Login(){
    if (!this.servicios.ValCam("Correo electrónico","Email",this.DatLog.Ema,"Si")){return}
    if (!this.servicios.ValCam("Contraseña","Texto",this.DatLog.Pas,"Si")){return}

    this.servicios.AccSobBDAA("LOGINCLI","Si","",this.DatLog.Ema+","+this.DatLog.Pas+",","","","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);

      this.DatLog.Ema = "";
      this.DatLog.Pas = "";

      if (Res.data.length > 0){
        window.localStorage.setItem("Token",Res.data[0].Token);
        switch(Res.data[0].Estatus){
          case "Activo":
            this.servicios.UsuMat = Res.data[0];
            this.router.navigate(["home"]);
          break;

          case "Nuevo":
            this.servicios.EnvMsgSim("Acceso","Usuario Nuevo, debe esperar la activación");
          break;
          
          case "Bloqueado":
            this.servicios.EnvMsgSim("Acceso","Usuario Bloqueado, por favor contacte a soporte para mas detalles");
          break;
        }
      }else{
        this.servicios.EnvMsgSim("Acceso","Correo electrónico o Contraseña invalida");
      }
    }).catch((err)=>{console.log(err)});
  }

  LogGoo(){
    this.googlePlus.login({}).then(result => {
      //this.servicios.EnvMsgSim(this.servicios.NomApp,JSON.stringify(result))
      this.servicios.UsuReg.Nombre = result.givenName;
      this.servicios.UsuReg.Apellido = result.familyName;
      this.servicios.UsuReg.Email = result.email;
      this.servicios.UsuReg.Pas = result.userId;
      this.servicios.UsuReg.ConPas = result.userId;

      let re = /\&/gi;
      result.imageUrl = result.imageUrl.replace(re,"[]");
      this.servicios.UsuReg.FotPer = result.imageUrl;
      this.servicios.UsuReg.LRS = "Si"
      this.router.navigate(["registro1"]);

        /*
        obj.email          // 'eddyverbruggen@gmail.com'
        obj.userId         // user id
        obj.displayName    // 'Eddy Verbruggen'
        obj.familyName     // 'Verbruggen'
        obj.givenName      // 'Eddy'
        obj.imageUrl       // 'http://link-to-my-profilepic.google.com'
        obj.idToken        // idToken that can be exchanged to verify user identity.
        obj.serverAuthCode // Auth code that can be exchanged for an access token and refresh token for offline access
        obj.accessToken    // OAuth2 access token
        */

    }).catch(err => {
      this.servicios.EnvMsgSim(this.servicios.NomApp,JSON.stringify(err))
    });
  }

  LogFac(){
    this.fb.login(['public_profile','email']).then((res: FacebookLoginResponse) => {
      //console.log(res);
      //this.getUserInfo(res.authResponse.userID)
      this.fb.api('me?fields=' + ['name', 'email', 'first_name', 'last_name', 'picture.type(large)'].join(), null).then((res: any) => {
        //this.setFacebookUserInfo(res)
  
        this.servicios.UsuReg.Nombre = res.first_name;
        this.servicios.UsuReg.Apellido = res.last_name;
        this.servicios.UsuReg.Email = res.email;
        this.servicios.UsuReg.Pas = res.id;
        this.servicios.UsuReg.ConPas = res.id;
  
        let re = /\&/gi;
        res.picture.data.url = res.picture.data.url.replace(re,"[]");
        this.servicios.UsuReg.FotPer = res.picture.data.url;
        this.servicios.UsuReg.LRS = "Si"
  
        this.router.navigate(["registro1"]);
      }).catch(e => {console.log(e)});
    }).catch(e => {console.log(e)});
  }


  MosOcuCon(input: any): any {
    input.type = input.type === 'password' ?  'text' : 'password';
    input.selectionStart = 999;
    input.selectionEnd = 999;
    this.MosPas = this.MosPas == "No" ? "Si" : "No";
  }

  OlvCon(){
    if (!this.servicios.ValCam("Correo electrónico","Email",this.DatLog.Ema,"Si")){return}
    this.RecCon = "Eje";
    this.servicios.OlvCla(this.DatLog.Ema).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      if (Res.estatus == "success"){
        this.servicios.EnvMsgSim("Acceso","Por favor verifique la bandeja de entrada de "+this.DatLog.Ema);
      }else{
        this.servicios.EnvMsgSim("Acceso","El Email "+this.DatLog.Ema+" no esta registrado en la base de datos");
      }

      this.RecCon = "";
    }).catch((err)=>{console.log(err)});
  }

  Registro(){
    this.router.navigate(["registro1"]);
  }
}
