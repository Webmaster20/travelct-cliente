import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-ayuda',
  templateUrl: './ayuda.page.html',
  styleUrls: ['./ayuda.page.scss'],
})
export class AyudaPage implements OnInit {

  constructor(
    public router: Router,
    public servicios: Servicios,
  ) { }

  ngOnInit() {
  }

  VerPre(){
    this.router.navigate(["prefre1"]);
  }
}
