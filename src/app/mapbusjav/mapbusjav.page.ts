import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Servicios } from 'src/servicios/servicios';
import { NavParams, Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

declare var google: any;

@Component({
  selector: 'app-mapbusjav',
  templateUrl: './mapbusjav.page.html',
  styleUrls: ['./mapbusjav.page.scss'],
})
export class MapbusjavPage implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  map: any;
  UsuMar: any;
  UsuAgr: string = "No";
  CliMar: any;
  LLCli:string = "";

  UsuMat: any = [];
  ApiImg: string = "";

  UbiMap: string = "";
  OriDes: string = "";

  Buscar: string = "";

  Llama: string = "";

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public servicios: Servicios,
    private platform: Platform,
    public locationX: Location,
  ){
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.OriDes = this.router.getCurrentNavigation().extras.state.OriDes;
        //console.log(this.OriDes)
      }
    });
  }

  async ngOnInit(){
    await this.platform.ready();
    await this.showMap(
      function(thiss){
        google.maps.event.trigger(thiss.map,"center_changed");
        //thiss.ObtUbi();
      }
    );
  }

  ionViewDidLoad() {
    this.UsuMat = this.servicios.UsuMat;
    this.ApiImg = this.servicios.ApiUrl+"img/";
  }

  SitSel(){
    if (document.getElementById("BusDes").innerHTML == ""){
      this.servicios.EnvMsgSim("Primero debe seleccionar el sitio de "+this.OriDes,"Advertencia");
      return;
    }

    this.UbiMap = this.CliMar.getPosition();
    this.UbiMap = this.UbiMap.toString();
    this.UbiMap = this.UbiMap.replace("(","");
    this.UbiMap = this.UbiMap.replace(")","");
    this.UbiMap = this.UbiMap.replace(" ","");

    if (this.OriDes == "Origen"){
      this.servicios.Origen.Origen = this.UbiMap;
      this.servicios.Origen.OriDes = document.getElementById("BusDes").innerHTML;
    }else if (this.OriDes == "Destino"){
      this.servicios.Destino.Destino = this.UbiMap;
      this.servicios.Destino.DesDes = document.getElementById("BusDes").innerHTML;
    }else if (this.OriDes == "Domicilio"){
      this.servicios.DirAct.LatLon = this.UbiMap;
      this.servicios.DirAct.Des = document.getElementById("BusDes").innerHTML;

      console.log(this.UbiMap+"|"+document.getElementById("BusDes").innerHTML)
    }else if (this.OriDes == "Parada"){
      this.servicios.DirAct.LatLon = this.UbiMap;
      this.servicios.DirAct.Des = document.getElementById("BusDes").innerHTML;

      console.log(this.UbiMap+"|"+document.getElementById("BusDes").innerHTML)
    }

    this.locationX.back();
  }

  showMap(callback){
    var location;

    if (this.servicios.LatLon.Com != ""){
      var ULLM = this.servicios.LatLon.Com.split(",");
      location = new google.maps.LatLng(parseFloat(ULLM[0]),parseFloat(ULLM[1]));
      console.log(parseFloat(ULLM[0])+"/"+parseFloat(ULLM[1]))
    }else{
      location = new google.maps.LatLng(this.servicios.LatLon.Lat,this.servicios.LatLon.Lon);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
    }

    const map = new google.maps.Map(this.mapRef.nativeElement, options);

    //const trafficLayer = new google.maps.TrafficLayer();
    //trafficLayer.setMap(map);

    google.maps.event.addListener(map, 'drag', function() {
      document.getElementById("DivCenEst").style.opacity = "1";
    });

    google.maps.event.addListener(map, 'center_changed', function() {
      window.setTimeout(function() {
       var center = map.getCenter();
       var cenmap = new google.maps.LatLng(center.lat(),center.lng());
       geocoder.geocode({'location': cenmap}, function(results, status) {
        if (status === 'OK'){
          if (results[0]) {
            infowindow.setContent("<p style='color:#000;'>"+results[0].formatted_address+"</p>");
            document.getElementById("BusDes").innerHTML = results[0].formatted_address;
            document.getElementById("BusLatLon").innerHTML = center.lat()+","+center.lng();
            CliMar.setPosition(cenmap);
            infowindow.open(map, CliMar);
            document.getElementById("DivCenEst").style.opacity = "0.1";
            //console.log(document.getElementById("BusDes").innerHTML);
          }else{
            //document.getElementById("BusDes").innerHTML = "Sin resultados";
          }
        }else{
          //document.getElementById("BusDes").innerHTML = "Sin resultados";
        }
      });
      }, 1000);
    });

    const CliMar = new google.maps.Marker({
      id: "IDMCSCli",
      position: location,
      map: map
    });
    CliMar.setVisible(false);

    const geocoder = new google.maps.Geocoder;

    var input = window.document.getElementById('pac-input');
    var infowindow = new google.maps.InfoWindow();
    var autocomplete = new google.maps.places.Autocomplete(input);
    
    /*
    map.addListener('click', function(e) {
      CliMar.setPosition(e.latLng);
      map.panTo(CliMar.getPosition());
      map.setZoom(16);
      infowindow.setContent("...");
      geocoder.geocode({'location': e.latLng}, function(results, status) {
        if (status === 'OK'){
          if (results[0]) {
            infowindow.setContent(results[0].formatted_address);
            document.getElementById("BusDes").innerHTML = results[0].formatted_address;
            document.getElementById("BusLatLon").innerHTML = e.latLng.lat()+","+e.latLng.lng();
            infowindow.open(map, CliMar);
          }else{
            document.getElementById("BusDes").innerHTML = "Sin resultados";
          }
        }else{
          document.getElementById("BusDes").innerHTML = "Sin resultados";
        }
      });
    });
    */

    autocomplete.bindTo('bounds', map);
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    infowindow.setContent("...");

    autocomplete.addListener('place_changed', function() {
      infowindow.close();
      CliMar.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }

      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
      CliMar.setPosition(place.geometry.location);
      CliMar.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      document.getElementById("BusDes").innerHTML = address;
      document.getElementById("BusLatLon").innerHTML = place.geometry.location.lat()+","+place.geometry.location.lng();

      infowindow.setContent(address);
      infowindow.open(map, CliMar);
    })

    this.map = map;
    this.CliMar = CliMar;
    
    callback(this);
  }

  ObtUbi(){
    this.addUsuMar(this.servicios.LatLon.Lat,this.servicios.LatLon.Lon);
  }

  addUsuMar(Lat,Lon){
    if (isNaN(Lat)){return;}
    if (isNaN(Lon)){return;}

    const map = this.map;
    const position = new google.maps.LatLng(Lat,Lon);

    if (this.UsuAgr == "No"){
      this.UsuAgr = "Si";
    }else{
      map.panTo(this.UsuMar.getPosition());
      map.setZoom(16);
      return;
    }

    var icon = {url: "assets/imgs/usuico.png"};

    this.UsuMar = new google.maps.Marker({
      position,
      map,
      icon: icon
    })

    this.CliMar.setPosition(this.UsuMar.getPosition());
    map.panTo(this.UsuMar.getPosition());
    map.setZoom(16);
  }

}