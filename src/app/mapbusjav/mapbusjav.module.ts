import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapbusjavPageRoutingModule } from './mapbusjav-routing.module';

import { MapbusjavPage } from './mapbusjav.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapbusjavPageRoutingModule
  ],
  declarations: [MapbusjavPage]
})
export class MapbusjavPageModule {}
