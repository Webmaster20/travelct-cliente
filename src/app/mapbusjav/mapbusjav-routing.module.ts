import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapbusjavPage } from './mapbusjav.page';

const routes: Routes = [
  {
    path: '',
    component: MapbusjavPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapbusjavPageRoutingModule {}
