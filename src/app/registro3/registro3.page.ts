import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { ModalController } from '@ionic/angular';
import { ModpagPage } from '../modpag/modpag.page';

@Component({
  selector: 'app-registro3',
  templateUrl: './registro3.page.html',
  styleUrls: ['./registro3.page.scss'],
})
export class Registro3Page implements OnInit {

  LisCat: any;

  DetFot: any = {
    "Carpeta":"usu",
    "Destino":"Registro"
  }

  ModRes: any = [];
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
    public modalController: ModalController

  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    /*
    this.servicios.AccSobBDAA("SELECT","Si","*","","","categorias","WHERE Estatus=|Activa|","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisCat = Res.data;
    }).catch((err)=>{console.log(err)});
    */

    if (this.servicios.UsuReg.LRS == "Si"){this.ConReg()}
  }

  ConReg(){
    if (!this.servicios.DebMod){if (this.servicios.UsuReg.FotPer == "fpdu.png"){this.servicios.EnvMsgSim("Registro","Por favor agregue una imagen para identificar su Cliente"); return;}}
    if (!this.servicios.ValCam("Nombre","Texto",this.servicios.UsuReg.Nombre,"Si")){return;}
    if (!this.servicios.ValCam("Apellido","Texto",this.servicios.UsuReg.Apellido,"Si")){return;}
    if (!this.servicios.ValCam("Correo electrónico","Email",this.servicios.UsuReg.Email,"Si")){return;}

    if (!this.servicios.ValCam("Contraseña","Texto",this.servicios.UsuReg.Pas,"Si")){return;}
    if (!this.servicios.ValCam("Confirmar contraseña","Texto",this.servicios.UsuReg.ConPas,"Si")){return;}
    if (this.servicios.UsuReg.Pas != this.servicios.UsuReg.ConPas){this.servicios.EnvMsgSim("Registro","Las contraseñas no coinciden");return;}

    //if (this.ModRes.length == 0){this.servicios.EnvMsgSim(this.servicios.NomApp,"Debe seleccionar al menos una Categoría"); return}

    let Campos = "Nombre,Apellido,Email,Telefono,Password,FotPer,Tipo,FecHorReg";
    let Valores = "|"+this.servicios.UsuReg.Nombre+"|,|"+this.servicios.UsuReg.Apellido+"|,|"+this.servicios.UsuReg.Email+"|,|"+this.servicios.UsuReg.NumMov+"|,|"+this.servicios.UsuReg.Pas+"|,|"+this.servicios.UsuReg.FotPer+"|,|Cliente|,NOW()"
    this.servicios.AccSobBDAA("INSERT","No",Campos,Valores,"","usuarios","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      //console.log(Campos+"|"+Valores);

      /*
      let NRNU = Res.lastinsertid;
      for (let n = 0; n < this.ModRes.length; n++) {
        this.servicios.AccSobBDAA("INSERT","No","NRegPro,NRegCat",NRNU+","+this.ModRes[n].NRegistro,"","procat","","","",this.MosLoa).then((dataRes)=>{
          let ResB: any = dataRes;
          console.log(ResB);
        }).catch((err)=>{console.log(err)});
      }
      */

      this.servicios.AccSobBDAA("LOGINCLI","Si","",this.servicios.UsuReg.Email+","+this.servicios.UsuReg.Pas+",","","","","","",this.MosLoa).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        if (Res.data.length > 0){
          window.localStorage.setItem("Token",Res.data[0].Token);
          this.servicios.UsuMat = Res.data[0];
          this.router.navigate(["home"]);
          this.servicios.EnvMsgSim("¡Bienvenido!","Gracias por ser parte de "+this.servicios.NomApp)
        }
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }

  async OpeMod(){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {"TipMod": "LisCat"}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null){
        this.ModRes = dataReturned.data;
        //console.log(this.ModRes);
        if (this.ModRes){
          for (let n = 0; n < this.ModRes.length; n++) {
            if (n == 0){
              this.servicios.UsuReg.DesCat = this.ModRes[n].Nombre
            }else{
              this.servicios.UsuReg.DesCat = this.servicios.UsuReg.DesCat + ", " + this.ModRes[n].Nombre
            }
          }
        }
      }
    });
    return await modal.present();
  }

  TomFot(){
    this.servicios.SelFot(this);
  }
}