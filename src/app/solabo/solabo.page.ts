import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-solabo',
  templateUrl: './solabo.page.html',
  styleUrls: ['./solabo.page.scss'],
})
export class SolaboPage implements OnInit {

  TipAbo: string = "";
  RefAbo: string = "";
  FecAbo: string = "";
  MonAbo: number;
  MonAboStr: string;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
    public locationX: Location,
  ) { }

  ngOnInit() {
  }

  Solicitar(){
    if (this.MonAbo == null || this.MonAbo < 1){this.servicios.EnvMsgSim("Abonos","El monto solicitado no puede ser menor a 1"); return;}

    if (!this.servicios.ValCam("Tipo","Texto",this.TipAbo,"Si")){return;}
    if (!this.servicios.ValCam("Referencia","Texto",this.RefAbo,"Si")){return;}
    if (!this.servicios.ValCam("Fecha","Texto",this.FecAbo,"Si")){return;}

    //this.FecAbo = this.servicios.ForFec(this.FecAbo.replace("T",""));

    let MFA = this.FecAbo.split("T");
    this.FecAbo = this.servicios.ForFec(MFA[0]);
    //2021-06-02T08:08:22.885-04:00

    this.MonAboStr = this.MonAbo.toString();
    this.MonAboStr = this.MonAboStr.replace("$","");
    this.MonAboStr = this.MonAboStr.replace(",",".");
    this.MonAbo = parseFloat(this.MonAboStr);

    this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Descripcion,Monto,FecHorReg",this.servicios.UsuMat.NRegistro+",|Abono "+this.TipAbo+" REF:"+this.RefAbo+" el "+this.FecAbo+"|,"+this.MonAbo+",NOW()","","solabo","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      this.servicios.EnvMsgToa("La solicitud fue registrada");
      this.locationX.back();

      this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Titulo,Contenido,Accion,Datos,FecHor","1,|Nuevo Abono|,|Nuevo abono fue solicitado|,|Billetera|,|"+Res.lastinsertid+"|,NOW()","","noti","","","",this.MosLoa).then((dataRes)=>{
        let ResD: any = dataRes;
        //console.log(ResD);
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }
}
