import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SolaboPage } from './solabo.page';

const routes: Routes = [
  {
    path: '',
    component: SolaboPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SolaboPageRoutingModule {}
