import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SolaboPageRoutingModule } from './solabo-routing.module';

import { SolaboPage } from './solabo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SolaboPageRoutingModule
  ],
  declarations: [SolaboPage]
})
export class SolaboPageModule {}
