import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SolserPageRoutingModule } from './solser-routing.module';

import { SolserPage } from './solser.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SolserPageRoutingModule
  ],
  declarations: [SolserPage]
})
export class SolserPageModule {}
