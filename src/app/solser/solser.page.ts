import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';
import { ModalController } from '@ionic/angular';
import { ModpagPage } from '../modpag/modpag.page';

declare var google: any;

@Component({
  selector: 'app-solser',
  templateUrl: './solser.page.html',
  styleUrls: ['./solser.page.scss'],
})
export class SolserPage implements OnInit {

  @ViewChild('map') mapRef: ElementRef;
  map: any;
  DirSer: any;
  DirDis: any;
  infwinOri: any;
  infwinDes: any;
  IcoOri: any;
  IcoDes: any;

  DatTar: any;

  Distancia: any = {
    "Tex":"",
    "Val":0
  }
  Tiempo: any = {
    "Tex":"",
    "Val":0
  }

  ForPag: string = "";
  ModRes: any;

  Cargando: string = "";

  constructor(
    public router: Router,
    public servicios: Servicios,
    private platform: Platform,
    public locationX: Location,
    public modalController: ModalController,
  ) { }

  async ngOnInit(){
    await this.platform.ready();
    await this.showMap(
      function(thiss){
        //console.log("Hola");
      }
    );
  }

  ionViewDidEnter(){
    if (this.servicios.Origen.Origen != "" && this.servicios.Destino.Destino != ""){
      this.MosRutMap(this.servicios.Origen.Origen,this.servicios.Destino.Destino,this,function(response,thiss){
        //console.log(response.routes[0].legs[0]);
        thiss.Distancia.Tex = response.routes[0].legs[0].distance.text;
        thiss.Distancia.Val = response.routes[0].legs[0].distance.value;
        thiss.Tiempo.Tex = response.routes[0].legs[0].duration.text;
        thiss.Tiempo.Val = response.routes[0].legs[0].duration.value;
        thiss.CalTarSer();
      });
    }
  }

  CalTarSer(){
    this.servicios.CalTarVia(this.Distancia.Val,this.Tiempo.Val).then((dataRes)=>{
      this.DatTar = dataRes;
      console.log(this.DatTar);
    }).catch((err)=>{console.log(err)});
  }

  RegVia(){
    let Campos = "";
    let Valores = "";
    Campos = "NRegCli,Origen,OriDes,OriZon,Destino,DesDes,DesZon,Distancia,Recorrido,Tiempo,Duracion,Precio,Ganancia,ForPag,FecHorSol";
    Valores = this.servicios.UsuMat.NRegistro+",|"+this.servicios.Origen.Origen+"|,|"+this.servicios.Origen.OriDes+"|,|"+this.DatTar.data.DesZonOri+"|,|"+this.servicios.Destino.Destino+"|,|"+this.servicios.Destino.DesDes+"|,|"+this.DatTar.data.DesZonDes+"|,|"+this.Distancia.Tex+"|,|"+this.Distancia.Val+"|,|"+this.Tiempo.Tex+"|,|"+this.Tiempo.Val+"|,|"+this.DatTar.data.MonTarTot+"|,|"+this.DatTar.data.MonGan+"|,|"+this.ForPag+"|,NOW()"
    this.servicios.AccSobBDAA("INSERT","No",Campos,Valores,"","viajes","","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgToa("El viaje fue solicitado ...")
      this.locationX.back();
    }).catch((err)=>{console.log(err)});
  }

  async OpeModPag(){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {"TipMod": "PagSer"}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null){
        this.ModRes = dataReturned.data;
        //console.log(this.ModRes);
        this.ForPag = this.ModRes;
      }
    });
    return await modal.present();
  }

  VerMap(OriDes){
    let data: NavigationExtras = {state:{OriDes:OriDes}};
    //this.router.navigate(["mapbus"],data);
    this.router.navigate(["mapbusjav"],data);
  }

  StyDivMap(): Object {
    if (this.servicios.Destino.DesDes == ""){
      return {
        width:"100%",
        height:"40%",
        opacity:"0",
        transition: "all 2s"
      }
    }else{
      return {
        width:"100%",
        height:"40%",
        opacity:"1",
        transition: "all 2s"
      }
    }
  }

  showMap(callback){
    var location;

    if (this.servicios.LatLon.Com != ""){
      var ULLM = this.servicios.LatLon.Com.split(",");
      location = new google.maps.LatLng(parseFloat(ULLM[0]),parseFloat(ULLM[1]));
      console.log(parseFloat(ULLM[0])+"/"+parseFloat(ULLM[1]))
    }else{
      location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
      },
    }

    const map = new google.maps.Map(this.mapRef.nativeElement, options);
    this.map = map;
  }

  MosRutMap(Ori,Des,thiss,callback){
    this.Cargando = "Si";

    if (this.DirDis){
      if (this.DirDis){this.DirDis.setMap(null);}
      this.DirSer = undefined
      this.DirDis = undefined
      this.IcoOri.setMap(null);
      this.IcoDes.setMap(null);
    }

    this.DirSer = new google.maps.DirectionsService();
    this.DirDis = new google.maps.DirectionsRenderer(
      {
        suppressMarkers: true,
        polylineOptions: {strokeColor:'#154c68',strokeOpacity:0.8,strokeWeight:7}
      }
    );

    var Waypts = [];

    this.DirDis.setMap(this.map);
    var dirdis = this.DirDis;
    this.AgrMarRut(Ori,Des);
    this.DirSer.route({
      origin: Ori,
      destination: Des,
      waypoints: Waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        dirdis.setDirections(response);
        callback(response,thiss);
        //console.log(response);
      }else{
        console.log('Directions request failed due to ' + status);
      }
    });
  }

  AgrMarRut(Ori,Des){
    var OriSt = String(Ori);
    var MatOri = OriSt.split(",");
    var posOri = new google.maps.LatLng(MatOri[0],MatOri[1]);

    var DesSt = String(Des);
    var MatDes = DesSt.split(",");
    var posDes = new google.maps.LatLng(MatDes[0],MatDes[1]);

    this.infwinOri = new google.maps.InfoWindow();
    this.infwinDes = new google.maps.InfoWindow();

    this.infwinOri.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#fff; text-align:center; vertical-align:middle;'>a<br>a</td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.servicios.Origen.OriDes+" <spam style='color:#555; font-size:16px; font-weight:bold;'>&nbsp;></spam></td></tr></table></div>");
    this.infwinDes.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#3270dd; text-align:center; vertical-align:middle;' id='tdDVx'></td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.servicios.Destino.DesDes+" <spam style='color:#555; font-size:16px; font-weight:bold;'>&nbsp;></spam></td></tr></table></div>");

    var icoori = {
      url: this.servicios.ApiUrl+"img/mapa/ori.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    var icodes = {
      url: this.servicios.ApiUrl+"img/mapa/des.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    this.IcoOri = new google.maps.Marker({
      position: posOri,
      map: this.map,
      icon: icoori,
      title: "Origen"
    });
    //this.infwinOri.open(this.map,this.IcoOri);

    this.IcoDes = new google.maps.Marker({
      position: posDes,
      map: this.map,
      icon: icodes,
      title: "Destino"
    });
    //this.infwinDes.open(this.map,this.IcoDes);
  }
}
