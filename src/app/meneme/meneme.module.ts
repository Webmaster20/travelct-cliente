import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MenemePageRoutingModule } from './meneme-routing.module';

import { MenemePage } from './meneme.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenemePageRoutingModule
  ],
  declarations: [MenemePage]
})
export class MenemePageModule {}
