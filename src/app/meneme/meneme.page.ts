import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { Location } from '@angular/common';

@Component({
  selector: 'app-meneme',
  templateUrl: './meneme.page.html',
  styleUrls: ['./meneme.page.scss'],
})
export class MenemePage implements OnInit {

  Men: any;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public servicios: Servicios,
    public locationX: Location
  ){
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.Men = this.router.getCurrentNavigation().extras.state.Men;
      }
    });
  }

  ngOnInit() {
  }

}
