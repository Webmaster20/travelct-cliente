import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenemePage } from './meneme.page';

const routes: Routes = [
  {
    path: '',
    component: MenemePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MenemePageRoutingModule {}
