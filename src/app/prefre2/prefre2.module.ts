import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Prefre2PageRoutingModule } from './prefre2-routing.module';

import { Prefre2Page } from './prefre2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Prefre2PageRoutingModule
  ],
  declarations: [Prefre2Page]
})
export class Prefre2PageModule {}
