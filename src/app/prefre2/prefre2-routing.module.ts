import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Prefre2Page } from './prefre2.page';

const routes: Routes = [
  {
    path: '',
    component: Prefre2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Prefre2PageRoutingModule {}
