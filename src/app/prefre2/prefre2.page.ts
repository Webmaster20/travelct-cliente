import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-prefre2',
  templateUrl: './prefre2.page.html',
  styleUrls: ['./prefre2.page.scss'],
})
export class Prefre2Page implements OnInit {

  TemAct: any;
  LisPre: any;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public servicios: Servicios,
  ){
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.TemAct = this.router.getCurrentNavigation().extras.state.Tem;
      }
    });
  }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarLisPre()
  }

  VerRes(Pre){
    this.servicios.EnvMsgSim(Pre.Tema+" > "+Pre.Pregunta,Pre.Respuesta);
  }

  CarLisPre(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","prefre","WHERE Usuario=|"+this.servicios.UsuMat.Tipo+"| AND Tema=|"+this.TemAct.Tema+"| AND Estatus=|Activa|","ORDER BY Tema","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      this.LisPre = Res.data;
    }).catch((err)=>{console.log(err)});
  }

}
