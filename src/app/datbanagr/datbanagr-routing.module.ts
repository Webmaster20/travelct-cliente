import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatbanagrPage } from './datbanagr.page';

const routes: Routes = [
  {
    path: '',
    component: DatbanagrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatbanagrPageRoutingModule {}
