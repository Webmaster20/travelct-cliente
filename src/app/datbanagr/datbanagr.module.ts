import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatbanagrPageRoutingModule } from './datbanagr-routing.module';

import { DatbanagrPage } from './datbanagr.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatbanagrPageRoutingModule
  ],
  declarations: [DatbanagrPage]
})
export class DatbanagrPageModule {}
