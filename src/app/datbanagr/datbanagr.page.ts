import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-datbanagr',
  templateUrl: './datbanagr.page.html',
  styleUrls: ['./datbanagr.page.scss'],
})
export class DatbanagrPage implements OnInit {

  Banco: string = "";
  Titular: string = "";
  ID: string = "";
  Cuenta: string = "";
  Tipo: string = "";

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
    public locationX: Location,
  ) { }

  ngOnInit() {
  }

  AgrDat(){
    if (!this.servicios.ValCam("Nombre del Banco","Texto",this.Banco,"Si")){return;}
    if (!this.servicios.ValCam("Nombre del titular","Texto",this.Titular,"Si")){return;}
    if (!this.servicios.ValCam("DNI o NIE","Texto",this.ID,"Si")){return;}
    if (!this.servicios.ValCam("Número de cuenta","Texto",this.Cuenta,"Si")){return;}
    if (!this.servicios.ValCam("Tipo de cuenta","Texto",this.Tipo,"Si")){return;}

    this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Banco,TitCue,ID,NumCue,Tipo","|"+this.servicios.UsuMat.NRegistro+"|,|"+this.Banco+"|,|"+this.Titular+"|,|"+this.ID+"|,|"+this.Cuenta+"|,|"+this.Tipo+"|","","datcue","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgToa("Datos agregados");
      this.locationX.back();
    }).catch((err)=>{console.log(err)});
  }
}
