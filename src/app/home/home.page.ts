import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { ModalController, Platform } from '@ionic/angular';
import { ModpagPage } from '../modpag/modpag.page';

declare var google;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('map',{static:true}) mapRef: ElementRef;
  map: any;
  marcadores: any = [];
  geocercas: any = [];
  rutas: any = [];
  limits: any;
  polyline: any;
  Ruta: any;
  LisCon: any;

  TimActPV: any;
  TimActVehPer: any;

  Saludo: string = "";
  MosLoa: boolean = true;
  ModRes: any;

  REVNOT: any;
  REVDAS: any;
  PriCar: boolean = false;

  ////////////////////////////////

  UsuMar: any;
  UsuAgr: string = "No";
  TUA: any;
  infwinUsu: any;

  ChoMar: any;
  ChoAgr: string = "No";
  TCA: any;
  infwinCho: any;
  AviChoCer: boolean = false;

  ////////////////////////////////

  DirSer: any;
  DirDis: any;
  infwinOri: any;
  infwinDes: any;
  infwinVen: any;
  IcoOri: any;
  IcoDes: any;

  DirSerCho: any;
  DirDisCho: any;
  TLC: string = ""

  Cargando: string = "";

  ////////////////////////////////

  ViaAct: any;
  CLBCAC: number = 3;
  IABCAC: number = 0;
  REVEVA: any;
  CalVia: number = 0;

  EncVia: any;

  ////////////////////////////////

  ARRJSDP: any = [
    /*
    {Titulo:"Publicidad 1",Descripcion:"Descripcion de la publicidad 1", Imagen:"fondo.png"},
    {Titulo:"Publicidad 2",Descripcion:"Descripcion de la publicidad 2", Imagen:"fondo.png"},
    {Titulo:"Publicidad 3",Descripcion:"Descripcion de la publicidad 3", Imagen:"fondo.png"},
    {Titulo:"Publicidad 4",Descripcion:"Descripcion de la publicidad 4", Imagen:"fondo.png"},
    {Titulo:"Publicidad 5",Descripcion:"Descripcion de la publicidad 5", Imagen:"fondo.png"},
    {Titulo:"Publicidad 6",Descripcion:"Descripcion de la publicidad 6", Imagen:"fondo.png"},
    {Titulo:"Publicidad 7",Descripcion:"Descripcion de la publicidad 7", Imagen:"fondo.png"},
    {Titulo:"Publicidad 8",Descripcion:"Descripcion de la publicidad 8", Imagen:"fondo.png"},
    {Titulo:"Publicidad 9",Descripcion:"Descripcion de la publicidad 9", Imagen:"fondo.png"},
    */
  ]

  PubEle: any = {Titulo:"",Descripcion:"", Imagen:""}

  constructor(
    private platform: Platform,
    public router: Router,
    public servicios: Servicios,
    public modalController: ModalController,
  ){
    this.GenSal();
  }

  ngAfterContentInit(){
    this.showMap(
      function(thiss){
        thiss.CarConCon();
        thiss.TUA = setInterval(() => thiss.addUsuMar(),1000);
      }
    );
  }

  ngOnInit(){
    this.platform.backButton.subscribe(() => {
      //console.log(this.router.url);
      if (this.router.url === '/home') {
        this.platform.backButton.unsubscribe();
        navigator['app'].exitApp();
      }
    })
  }

  ionViewDidEnter(){
    let TokFCM = window.localStorage.getItem("TPN");
    this.servicios.AccSobBDAA("UPDATE","No","","","TokFCM=|"+TokFCM+"|","usuarios","WHERE NRegistro="+this.servicios.UsuMat.NRegistro,"","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
    }).catch((err)=>{console.log(err)});
    
    this.GenSal();
    this.CarParGen();
    this.CarEncVia();

    this.RevNot();
    this.RevDas();
    if (!this.REVNOT){this.REVNOT = setInterval(() => this.RevNot(),10000);}
    if (!this.REVDAS){this.REVDAS = setInterval(() => this.RevDas(),10000);}
    if (!this.PriCar){this.PriCar=true; this.SalLog();}

    this.CarViaAct();
  }

  CarViaAct(){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro=A.NRegCho) NomCon,(SELECT UbiAct FROM usuarios WHERE NRegistro=A.NRegCho) UbiCon,(SELECT Telefono FROM usuarios WHERE NRegistro=A.NRegCho) TelCon,(SELECT FotPer FROM usuarios WHERE NRegistro=A.NRegCho) FotCon,(SELECT Puntos FROM usuarios WHERE NRegistro=A.NRegCho) PunCon,(SELECT Tipo FROM vehiculos WHERE NRegUsu=A.NRegCho LIMIT 1) TipVeh,(SELECT Marca FROM vehiculos WHERE NRegUsu=A.NRegCho LIMIT 1) MarVeh,(SELECT Modelo FROM vehiculos WHERE NRegUsu=A.NRegCho LIMIT 1) ModVeh,(SELECT Matricula FROM vehiculos WHERE NRegUsu=A.NRegCho LIMIT 1) MatVeh","","","viajes A","WHERE NRegCli="+this.servicios.UsuMat.NRegistro+" AND Estatus IN (|Nuevo|,|Aceptado|,|Iniciado|,|Finalizado|,|Calificado Conductor|)","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.ViaAct = Res.data;

      if (this.ViaAct.length > 0){
        this.LimMapConCon();
        switch(this.ViaAct[0].Estatus){
          case "Nuevo":
            this.IABCAC = 0;
            this.BusConDis();
          break;
          case "Aceptado":
            if (!this.REVEVA){this.REVEVA = setInterval(() => this.CarViaAct(),3000);}
            if (!this.TCA){this.TCA = setInterval(() => this.addChoMar(),1000);}

            if (!this.DirSer){
              this.MosRutMap(this.ViaAct[0].Origen,this.ViaAct[0].Destino,this,function(response,thiss){
                //console.log(response.routes[0].legs[0]);
              });
              this.AddChoRut(this,function(response,thiss){
                //console.log(response.routes[0].legs[0].duration.text);
                thiss.TLC = response.routes[0].legs[0].duration.text;
              });
            }
          break;
          case "Iniciado":
            if (!this.REVEVA){this.REVEVA = setInterval(() => this.CarViaAct(),3000);}
            if (!this.TCA){this.TCA = setInterval(() => this.addChoMar(),1000);}

            if (!this.DirSer){
              this.MosRutMap(this.ViaAct[0].Origen,this.ViaAct[0].Destino,this,function(response,thiss){
                //console.log(response.routes[0].legs[0]);
              });
              this.AddChoRut(this,function(response,thiss){
                //console.log(response.routes[0].legs[0].duration.text);
                thiss.TLC = response.routes[0].legs[0].duration.text;
              });
            }
          break;
          case "Finalizado":
            if (!this.REVEVA){this.REVEVA = setInterval(() => this.CarViaAct(),3000);}
            this.LimMapCho();
            this.LimMapRP();
            this.LimMapRR();
            this.CarConCon();
          break;
          case "Calificado Conductor":
            if (!this.REVEVA){this.REVEVA = setInterval(() => this.CarViaAct(),3000);}
            this.LimMapCho();
            this.LimMapRP();
            this.LimMapRR();
            this.CarConCon();
          break;
        }
      }else{
        //////////////AQUI LA PROGRAMACION DEL ESTATUS EN REPOSO DE LA APP////////////////
        this.CarConCon();
        this.LimMapCho();
        this.LimMapRP();
        this.LimMapRR();
      }
    }).catch((err)=>{console.log(err)});
  }

  Calificar(){
    let PunCho = this.CalVia * 20;
    this.servicios.AccSobBDAA("UPDATE","No","","","PunCalCho=|"+PunCho+"|,Estatus=|Terminado|","viajes","WHERE NRegistro="+this.ViaAct[0].NRegistro,"","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
        this.servicios.AccSobBDAA("SELECT","Si","AVG(PunCalCho) ProCal","","","viajes","WHERE NRegCho = "+this.ViaAct[0].NRegCho+" AND Estatus IN (|Terminado|,|Calificado Cliente|)","","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          console.log(Res);
          this.servicios.AccSobBDAA("UPDATE","No","","","Puntos=|"+Res.data[0].ProCal+"|","usuarios","WHERE NRegistro="+this.ViaAct[0].NRegCho,"","",this.MosLoa).then((dataRes)=>{
            let ResD: any = dataRes;
            console.log(ResD);
            this.servicios.EnvMsgSim("Viajes","El Viaje fue Calificado");

            for (let n = 0; n < this.EncVia.length; n++) {
              if (this.EncVia[n].OpcSel != ""){
                this.servicios.AccSobBDAA("INSERT","No","NRegVia,Pregunta,Respuesta,FecHorReg",this.ViaAct[0].NRegistro+",|"+this.EncVia[n].Pregunta+"|,|"+this.EncVia[n].OpcSel+"|,NOW()","","viajesenc","","","",this.MosLoa).then((dataRes)=>{
                  let ResD: any = dataRes;
                  //console.log(ResD);
                }).catch((err)=>{console.log(err.message)});
              }
            }

            this.CarViaAct();
          }).catch((err)=>{console.log(err)});
        }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }

  ///////////////////////////////////////////////////////////////
  BusConDis(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","viajes A","WHERE NRegCli="+this.servicios.UsuMat.NRegistro+" AND Estatus=|Nuevo| AND NRegistro NOT IN (SELECT NRegVia FROM invvia WHERE Estatus = |Nueva| AND FechaHora BETWEEN (NOW() - INTERVAL 1 MINUTE) AND NOW()) ","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      if (Res.data.length > 0){
        this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT COUNT(*) FROM viajes WHERE NRegCho = A.NRegistro AND FecHorSol BETWEEN (NOW() - INTERVAL 1 DAY) AND NOW()) TotViaHoy","","","usuarios A","WHERE Tipo=|Conductor| AND EstCon=|Conectado|","ORDER BY NRegistro","",false).then((dataRes)=>{
          let ResB: any = dataRes;
          console.log(ResB);
          if (ResB.data.length > 0){
            let CMP: number = 0;
            let NRMP: number = 0;
            for (let n = 0; n < ResB.data.length; n++) {
              if (n == 0){
                CMP = parseInt(ResB.data[n].TotViaHoy);
                NRMP = n;
              }
              if (CMP > parseInt(ResB.data[n].TotViaHoy)){
                CMP = parseInt(ResB.data[n].TotViaHoy);
                NRMP = n;
              }
            }
            this.InvRep(Res.data[0],ResB.data[NRMP]);
          }else{
            this.IABCAC++;
            console.log(this.IABCAC);
            if (this.IABCAC > this.CLBCAC){
              this.IABCAC = 0;
              this.servicios.EnvMsgToa("Disculpe en este momento no tenemos conductores disponibles");
              this.CanVia("No se consiguieron conductores");
            }else{
              if (this.IABCAC > 1){this.servicios.EnvMsgToa("Seguimos buscando un conductor disponible, gracias por su paciencia ...");}
              setTimeout(() => {this.BusConDis()},30000);
            }
          }
        }).catch((err)=>{console.log(err)});
      }
    }).catch((err)=>{console.log(err)});
  }

  InvRep(ViaNue,ChoInv){
    this.servicios.AccSobBDAA("UPDATE","No","","","EstCon=|Invitado|","usuarios","WHERE NRegistro="+ChoInv.NRegistro,"","",this.MosLoa).then((dataRes)=>{
      let ResB: any = dataRes;
      console.log(ResB);
      this.servicios.AccSobBDAA("INSERT","No","NRegVia,NRegCho,FechaHora",ViaNue.NRegistro+","+ChoInv.NRegistro+",NOW()","","invvia","","","",this.MosLoa).then((dataRes)=>{
        let ResC: any = dataRes;
        console.log(ResC);

        this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Titulo,Contenido,Accion,Datos,FecHor",ChoInv.NRegistro+",|Viajes|,|Nuevo Viaje|,|Viajes|,|"+ViaNue.NRegistro+"|,NOW()","","noti","","","",this.MosLoa).then((dataRes)=>{
          let ResD: any = dataRes;
          //console.log(ResD);

          this.servicios.EnvNotPus(ChoInv.NRegistro,"Viajes","Nuevo Viaje","Viajes",ViaNue.NRegistro).then((dataRes)=>{
            let ResE: any = dataRes;
            //console.log(ResE);
          }).catch((err)=>{console.log(err)});
        }).catch((err)=>{console.log(err.message)});
        
        setTimeout(() => {this.RevInvEnv(ResC.lastinsertid)},20000);

      }).catch((err)=>{console.log(err.message)});
    }).catch((err)=>{console.log(err.message)});
  }

  RevInvEnv(NRegInv){
    //this.servicios.EnvMsgToa("Revisando invitaciones ...");
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT COUNT(*) FROM invvia WHERE NRegVia=A.NRegVia AND NRegCho = A.NRegCho) TotInvEnv,(SELECT Nombre FROM usuarios WHERE NRegistro = A.NRegCho) NomRep","","","invvia A","WHERE NRegistro IN ("+NRegInv+")","","",false).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log("*********Revisando;*********")
      //console.log(Res);
      if (Res.data[0].Estatus != "Aceptada"){
        let NueEstRep = "Conectado"
        if (parseInt(Res.data[0].TotInvEnv) > 1){NueEstRep = "Desconectado"}

        this.servicios.AccSobBDAA("UPDATE","No","","","EstCon=|"+NueEstRep+"|","usuarios","WHERE NRegistro="+Res.data[0].NRegCho,"","",this.MosLoa).then((dataRes)=>{
          let ResB: any = dataRes;
          //console.log(Res);
          this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Sin respuesta|","invvia","WHERE NRegistro="+Res.data[0].NRegistro,"","",this.MosLoa).then((dataRes)=>{
            let ResC: any = dataRes;
            //console.log(Res);
          }).catch((err)=>{console.log(err.message)});
        }).catch((err)=>{console.log(err.message)});
      }else{
        if (!this.REVEVA){this.REVEVA = setInterval(() => this.CarViaAct(),3000);}
      }
    }).catch((err)=>{console.log(err.message)});
  }
  ///////////////////////////////////////////////////////////////

  CanVia(RazCan){
    this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Cancelado Cliente|,RazCan=|"+RazCan+"|","viajes","WHERE NRegistro="+this.ViaAct[0].NRegistro,"","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.AccSobBDAA("UPDATE","No","","","EstCon=|Conectado|","usuarios","WHERE NRegistro="+this.ViaAct[0].NRegCho,"","",this.MosLoa).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Titulo,Contenido,Accion,Datos,FecHor",this.ViaAct[0].NRegCho+",|Viajes|,|El Viaje fue cancelado|,|Viajes|,|"+this.ViaAct[0].NRegistro+"|,NOW()","","noti","","","",this.MosLoa).then((dataRes)=>{
          let ResB: any = dataRes;
          //console.log(ResB);
          this.servicios.EnvNotPus(this.ViaAct[0].NRegCho,"Viajes","El Viaje fue cancelado","Viajes",this.ViaAct[0].NRegistro).then((dataRes)=>{
            let ResC: any = dataRes;
            //console.log(ResC);

            this.ViaAct = [];
            this.LimMapCho();
            this.LimMapConCon();
            this.LimMapRP();
            this.LimMapRR();

            this.servicios.EnvMsgSim(this.servicios.NomApp,"El Viaje fue cancelado");
          }).catch((err)=>{console.log(err)});
        }).catch((err)=>{console.log(err)});
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }

  ///////////////////////////////////////////////////////////////

  async OpeModCanSol(){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {"TipMod": "CanSol"}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null){
        this.ModRes = dataReturned.data;
        //console.log(this.ModRes);
        if (this.ModRes){
          this.CanVia(this.ModRes.Des);
        }
      }
    });
    return await modal.present();
  }


  MueCha(){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegCho) NomApe,(SELECT Puntos FROM usuarios WHERE NRegistro = A.NRegCho) Puntos,(SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegCho) FotPer","","","viajes A","WHERE NRegistro = "+this.ViaAct[0].NRegistro+"","ORDER BY FecHorSol DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      let data: NavigationExtras = {state:{Ped:Res.data[0]}};
      this.router.navigate(["chat"],data);
    }).catch((err)=>{console.log(err)});
  }


  RevDas(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","publicidad","WHERE Estatus=|Activa|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.ARRJSDP = Res.data;

      let NIPA = Math.floor(Math.random() * ((this.ARRJSDP.length+1) - 0)) + 0;
      if (this.ARRJSDP[NIPA]){
        document.getElementById("DivSup").style.opacity="0";
        setTimeout(() => {
          this.PubEle = this.ARRJSDP[NIPA];
          document.getElementById("DivSup").style.opacity="1";
        }, 1000);
      }
    }).catch((err)=>{console.log(err)});
  }

  SolSer(){
    this.router.navigate(["solser"]);
  }

  VerUnd(Est){
    let data: NavigationExtras = {state:{Est:Est}};
    this.router.navigate(["unidades"],data);
  }
  VerRut(Est){
    let data: NavigationExtras = {state:{Est:Est}};
    this.router.navigate(["rutas"],data);
  }
  VerSub(){
    this.router.navigate(["solicitudessub"]);
  }

  VerMap(OriDes){
    let data: NavigationExtras = {state:{OriDes:OriDes}};
    this.router.navigate(["mapbusjav"],data);
  }

  SalLog(){
    let data: NavigationExtras = {state:{Men:{LogTra:this.servicios.UsuMat.FotPer,Titulo:this.servicios.UsuMat.Nombre,Mensaje:"¡Bienvenido! Gracias por usar "+this.servicios.NomApp}}};
    this.router.navigate(["meneme"],data);
  }

  RevNot(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","noti","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Estatus=|Nueva|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      if (Res.data.length > 0){

        for (let n = 0; n < Res.data.length; n++) {
          this.servicios.EnvLocNot(Res.data[n].NRegistro,Res.data[n].Titulo,Res.data[n].Contenido,Res.data[n].Accion,Res.data[n].Datos);

          switch (Res.data[n].Accion){
            case "Solicitudes":
              this.servicios.DatNot.Solicitudes.push(Res.data[n]);
            break;
            case "Servicios":
              this.servicios.DatNot.Servicios.push(Res.data[n]);
            break;
            case "Conductores":
              this.servicios.DatNot.Conductores.push(Res.data[n]);
            break;
            case "Chat":
              this.servicios.DatNot.Chat.push(Res.data[n]);
            break;
            case "Soporte":
              this.servicios.DatNot.Soporte.push(Res.data[n]);
            break;
            case "Panico":
              this.servicios.DatNot.Panico.push(Res.data[n]);
            break;
          }
        }

        this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Vista|","noti","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Estatus=|Nueva|","","",this.MosLoa).then((dataRes)=>{
          let ResB: any = dataRes;
          //console.log(Res);
        }).catch((err)=>{console.log(err)});
      }
    }).catch((err)=>{console.log(err)});

    this.servicios.AccSobBDAA("SELECT","Si","*","","","meneme","WHERE FecHor } (NOW() - INTERVAL 1 DAY) AND Estatus=|Nuevo| AND NRegUsu="+this.servicios.UsuMat.NRegistro,"ORDER BY NRegistro LIMIT 1","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      if (Res.data.length > 0){
        let data: NavigationExtras = {state:{Men:Res.data[0]}};
        this.router.navigate(["meneme"],data);
        this.servicios.AccSobBDAA("UPDATE","No","","","Estatus=|Visto|","meneme","WHERE NRegistro="+Res.data[0].NRegistro+"","","",this.MosLoa).then((dataRes)=>{
          let ResB: any = dataRes;
          //console.log(Res);
        }).catch((err)=>{console.log(err)});
      }
    }).catch((err)=>{console.log(err)});
  }

  GenSal(){
    var Ahora = new Date();
    var Hora = Ahora.getHours();
    if(Hora < 12){this.Saludo = "Buenos días";}
    if(Hora > 11 && Hora < 18){this.Saludo = "Buenas tardes";}
    if(Hora > 17 && Hora < 24){this.Saludo = "Buenas noches";}
  }

  CarParGen(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","parametros","","ORDER BY NRegistro","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.ParGen = Res.data;
    }).catch((err)=>{console.log(err)});
  }

  CarEncVia(){
    this.servicios.AccSobBDAA("SELECT","Si","*,|| Opcs,|| OpcSel","","","encvia","WHERE Usuario=|Cliente|","ORDER BY NRegistro","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.EncVia = Res.data;
      for (let n = 0; n < this.EncVia.length; n++) {
        this.EncVia[n].Opcs = this.EncVia[n].Opciones.split("-");
      }
    }).catch((err)=>{console.log(err)});
  }

  async OpeModEnc(Pre){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {TipMod: "EncVia",Pre:Pre}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned != null){
        this.ModRes = dataReturned.data;
        console.log(this.ModRes);
        if (this.ModRes){
          Pre.OpcSel = this.ModRes;
        }
      }
    });
    return await modal.present();
  }

  //////////////////////////////////////////////////////////////////////////////////////////////////

  showMap(callback){
    var location;
    if (this.servicios.UsuMat.UbiAct == ""){
      location = new google.maps.LatLng(this.servicios.Pais.Lat,this.servicios.Pais.Lon);
    }else{
      let MLLUA = this.servicios.UsuMat.UbiAct.split(",");
      location = new google.maps.LatLng(MLLUA[0],MLLUA[1]);
    }

    const options = {
      center: location,
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false,
      zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_TOP
      },
      fullscreenControl: false,
      fullscreenControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
      },
      styles: [
        //{featureType: 'poi',stylers: [{visibility: 'off'}]},
        //{featureType: 'transit.station',stylers: [{visibility: 'off'}]}
      ]
    }

    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    const trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(this.map);

    callback(this);
  }

  LimMapConCon(){
    if (this.marcadores.length == 0){return}

    this.map.panTo(this.UsuMar.getPosition());
    this.map.setZoom(13);

    clearInterval(this.TimActPV);
    for (let n = 0; n < this.marcadores.length; n++) {
      this.marcadores[n].setMap(null);
    }
    while(this.marcadores.length > 0){this.marcadores.pop();};
  }
  CarConCon(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE Tipo=|Conductor| AND EstCon=|Conectado|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.limits = new google.maps.LatLngBounds();
      this.LisCon = Res.data;
      for (var n=0; n<this.LisCon.length; n++){
        let MLL = this.LisCon[n].UbiAct.split(",");
        this.limits.extend(new google.maps.LatLng(MLL[0],MLL[1]));
        this.map.fitBounds(this.limits);

        this.CreMarCar(this.LisCon[n]);
      }
      if (!this.TimActPV){this.TimActPV = setInterval(() => this.ActPosMarker(),30000);}
    }).catch((err)=>{console.log(err)});
  }
  CreMarCar(Car){
    var MLL = Car.UbiAct.split(",");
    var point = new google.maps.LatLng(MLL[0],MLL[1]);

    var icon = {
      url: "assets/img/logo.png",
      scaledSize: new google.maps.Size(48, 48),
      rotation:0,
    };

    var marker = new google.maps.Marker({
      title: Car.Nombre+" "+Car.Apellido,
      id: Car.NRegistro,
      position: point,
      map: this.map,
      optimized: false,
      icon: icon
    });
    this.marcadores.push(marker);

    var contentString = "";
    contentString = contentString + "<div class='DivGloGen'><center>";
      contentString = contentString + "<div class='DivGloVeh'>";
        contentString = contentString + "<table class='tabGloVeh' cellpadding='1' cellspacing='1'>";
          contentString = contentString + "<tr>";
            contentString = contentString + "<td class='tdGloVeh'>"+Car.Nombre+" "+Car.Apellido+"</td>";
          contentString = contentString + "</tr>";
        contentString = contentString + "</table>";
      contentString = contentString + "</div>";
    contentString = contentString + "</center></div>";

    var infowindow = new google.maps.InfoWindow({content: contentString});
    google.maps.event.addListener(marker,'click',function(){infowindow.open(this.map,marker);});
    google.maps.event.addListener(infowindow,'closeclick',function(){});
  }
  ActPosMarker(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE Tipo=|Conductor| AND EstCon=|Conectado|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      for (var n = 0; n < this.marcadores.length; n++){
        for (var n2 = 0; n2 < Res.data.length; n2++){
          if (this.marcadores[n].id == Res.data[n2].NRegistro){
            var point = new google.maps.LatLng(Res.data[n2].Lat,Res.data[n2].Lon);
            this.marcadores[n].setPosition(point);
          }
        }
      }
    }).catch((err)=>{console.log(err)});
  }

  /////////////////////////////////////////////////////////////////////

  addUsuMar(){
    var FotMap = "";
    if (this.servicios.UsuMat.FotPer.includes('https://')){
      FotMap = this.servicios.UsuMat.FotPer;
    }else{
      FotMap = this.servicios.ApiUrl+"img/usu/"+this.servicios.UsuMat.FotPer
    }

    var position = new google.maps.LatLng(this.servicios.LatLon.Lat,this.servicios.LatLon.Lon);
    var iconCli = {
      url: FotMap,
      scaledSize: new google.maps.Size(36, 36),
      rotation:0,
    };

    if (this.UsuAgr == "No"){
      this.UsuMar = new google.maps.Marker({
        title: "Yo",
        position: position,
        rotation: this.servicios.LatLon.Ori,
        map: this.map,
        optimized: false,
        icon: iconCli,
        strokeColor: '#f00',
        strokeWeight: 5
      });

      this.infwinUsu = new google.maps.InfoWindow();
      this.infwinUsu.setContent("<p style='font-size:16px; color:#555; margin:5px;'><span style='text-transform:capitalize;'>"+this.servicios.UsuMat.Nombre+"</span></p>");
      this.infwinUsu.open(this.map,this.UsuMar);

      this.map.panTo(this.UsuMar.getPosition());
      this.map.setZoom(13);
      this.UsuAgr = "Si";
    }else{
      this.UsuMar.setPosition(position);
      this.OriMarCli("MarChoIco001",this.servicios.LatLon.Ori);
    }
  }
  OriMarCli(ID,Ori){
    if (document.getElementById(ID) == undefined){return;}
    //setTimeout(() => {document.getElementById(ID).style.transform = "rotate("+Ori+"deg)";},100);
  }

  /////////////////////////////////////////////////////////////////////
  LimMapCho(){
    clearInterval(this.TCA);
    if(this.ChoMar){this.ChoMar.setMap(null);}
  }
  addChoMar(){
    let MLLC = this.ViaAct[0].UbiCon.split(",");

    if (!this.AviChoCer){
      let DAECC = this.servicios.CalDisEntCooMts(this.servicios.LatLon.Lat,this.servicios.LatLon.Lon,MLLC[0],MLLC[1]);
      let PDAECC = 300;
      for (let n = 0; n < this.servicios.ParGen.length; n++) {
        if (this.servicios.ParGen[n].Clave == "DAECC"){
          PDAECC = this.servicios.ParGen[n].Valor;
        }
      }
      if (parseFloat(DAECC) < PDAECC){
        this.AviChoCer = true;
        this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Titulo,Contenido,Accion,Datos,FecHor",this.servicios.UsuMat.NRegistro+",|Viajes|,|El Conductor esta cerca|,|Viajes|,|"+this.ViaAct[0].NRegistro+"|,NOW()","","noti","","","",this.MosLoa).then((dataRes)=>{
          let ResD: any = dataRes;
          //console.log(ResD);

          this.servicios.EnvNotPus(this.servicios.UsuMat.NRegistro,"Viajes","El Conductor esta cerca","Viajes",this.ViaAct[0].NRegistro).then((dataRes)=>{
            let ResE: any = dataRes;
            //console.log(ResE);
          }).catch((err)=>{console.log(err)});
        }).catch((err)=>{console.log(err.message)});
      }
    }

    var position = new google.maps.LatLng(MLLC[0],MLLC[1]);
    var iconCli = {
      url: this.servicios.ApiUrl+"img/usu/"+this.ViaAct[0].FotCon,//////////////////////////
      scaledSize: new google.maps.Size(36, 36),
      rotation:0,
    };

    if (this.ChoAgr == "No"){
      this.ChoMar = new google.maps.Marker({
        title: "Yo",
        position: position,
        rotation: this.servicios.LatLon.Ori,
        map: this.map,
        optimized: false,
        icon: iconCli,
        strokeColor: '#f00',
        strokeWeight: 5
      });

      this.infwinCho = new google.maps.InfoWindow();
      this.infwinCho.setContent("<p style='font-size:16px; color:#555; margin:5px;'><strong>Conductor:</strong><br><span style='text-transform:capitalize;'>"+this.ViaAct[0].NomCon+"</span></p>");
      this.infwinCho.open(this.map,this.ChoMar);

      this.map.panTo(this.ChoMar.getPosition());
      this.map.setZoom(13);
      this.ChoAgr = "Si";
    }else{
      this.ChoMar.setPosition(position);
      this.OriMarCho("MarChoIco001",this.servicios.LatLon.Ori);
    }
  }
  OriMarCho(ID,Ori){
    if (document.getElementById(ID) == undefined){return;}
    //setTimeout(() => {document.getElementById(ID).style.transform = "rotate("+Ori+"deg)";},100);
  }




  ////////////////////////////////////////////////////////////////////////////////////////

  LimMapRP(){
    if (this.DirDis){
      if (this.DirDis){this.DirDis.setMap(null);}
      this.DirSer = undefined
      this.DirDis = undefined
      this.IcoOri.setMap(null);
      this.IcoDes.setMap(null);
    }
  }

  MosRutMap(Ori,Des,thiss,callback){
    this.Cargando = "Si";

    this.LimMapRP();

    this.DirSer = new google.maps.DirectionsService();
    this.DirDis = new google.maps.DirectionsRenderer(
      {
        suppressMarkers: true,
        polylineOptions: {strokeColor:'#154c68',strokeOpacity:0.8,strokeWeight:7}
      }
    );

    var Waypts = [];

    this.DirDis.setMap(this.map);
    var dirdis = this.DirDis;
    this.AgrMarRut(Ori,Des);
    this.DirSer.route({
      origin: Ori,
      destination: Des,
      waypoints: Waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        dirdis.setDirections(response);
        callback(response,thiss);
        //console.log(response);
      }else{
        //console.log('Directions request failed due to ' + status);
      }
    });
  }

  AgrMarRut(Ori,Des){
    var OriSt = String(Ori);
    var MatOri = OriSt.split(",");
    var posOri = new google.maps.LatLng(MatOri[0],MatOri[1]);

    var DesSt = String(Des);
    var MatDes = DesSt.split(",");
    var posDes = new google.maps.LatLng(MatDes[0],MatDes[1]);

    this.infwinOri = new google.maps.InfoWindow();
    this.infwinDes = new google.maps.InfoWindow();

    //this.infwinOri.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#fff; text-align:center; vertical-align:middle;'>a<br>a</td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.servicios.Origen.OriDes+" <spam style='color:#555; font-size:16px; font-weight:bold;'>&nbsp;></spam></td></tr></table></div>");
    //this.infwinDes.setContent("<div style='color:#fff;'><table style='whidth:100%;height:100%'><tr><td style='padding:3px; background:#fff;color:#3270dd; text-align:center; vertical-align:middle;' id='tdDVx'></td><td style='padding:3px; text-align:center; vertical-align:middle;'>&nbsp;"+this.servicios.Destino.DesDes+" <spam style='color:#555; font-size:16px; font-weight:bold;'>&nbsp;></spam></td></tr></table></div>");
    this.infwinOri.setContent("<p style='font-size:12px; color:#555; margin:3px;'><strong>Origen</strong></p>");
    this.infwinDes.setContent("<p style='font-size:12px; color:#555; margin:3px;'><strong>Destino</strong></p>");

    var icoori = {
      url: this.servicios.ApiUrl+"img/mapa/ori.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    var icodes = {
      url: this.servicios.ApiUrl+"img/mapa/des.png",
      scaledSize: new google.maps.Size(24,24), // scaled size
      origin: new google.maps.Point(0,0), // origin
      anchor: new google.maps.Point(12,12) // anchor
    };

    this.IcoOri = new google.maps.Marker({
      position: posOri,
      map: this.map,
      icon: icoori,
      title: "Origen"
    });
    this.infwinOri.open(this.map,this.IcoOri);

    this.IcoDes = new google.maps.Marker({
      position: posDes,
      map: this.map,
      icon: icodes,
      title: "Destino"
    });
    this.infwinDes.open(this.map,this.IcoDes);
  }


  LimMapRR(){
    if (this.DirDisCho){
      if (this.DirDisCho){this.DirDisCho.setMap(null);}
      this.DirSerCho = undefined
      this.DirDisCho = undefined
    }
  }

  AddChoRut(thiss,callback){
    
    this.LimMapRR();

    this.DirSerCho = new google.maps.DirectionsService();
    this.DirDisCho = new google.maps.DirectionsRenderer({
        suppressMarkers: true,
        polylineOptions: {strokeColor:'#ff0017',strokeOpacity:0.8,strokeWeight:7}
      }
    );

    var position = new google.maps.LatLng(this.servicios.LatLon.Lat,this.servicios.LatLon.Lon);
    var Ori = position;
    var Des = this.ViaAct[0].Origen;
    //console.log(Ori+"|"+Des);

    var Waypts = [];

    var dirdischo = this.DirDisCho;
    this.DirDisCho.setMap(this.map);
    this.DirSerCho.route({
      origin: Ori,
      destination: Des,
      waypoints: Waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        dirdischo.setDirections(response);
        //console.log(response);
        callback(response,thiss);

        /*
        var SumTie = response.routes[0].legs[0].duration.value;
        SumTie = SumTie / 60;
        SumTie = SumTie.toFixed(0);
        document.getElementById("DuracionCho").innerHTML = SumTie + " Min";
        */
      }else{
        //console.log('Directions request failed due to ' + status);
      }
    });
  }
}