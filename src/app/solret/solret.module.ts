import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SolretPageRoutingModule } from './solret-routing.module';

import { SolretPage } from './solret.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SolretPageRoutingModule
  ],
  declarations: [SolretPage]
})
export class SolretPageModule {}
