import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-solret',
  templateUrl: './solret.page.html',
  styleUrls: ['./solret.page.scss'],
})
export class SolretPage implements OnInit {

  MonRet: number;
  MonRetStr: string;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
    public locationX: Location,
  ) { }

  ngOnInit() {
  }

  Solicitar(){
    if (this.MonRet > parseFloat(this.servicios.UsuMat.Saldo)){this.servicios.EnvMsgSim("Retiros","El monto solicitado no puede ser mayor al Saldo"); return;}
    if (this.MonRet == null || this.MonRet < 1){this.servicios.EnvMsgSim("Retiros","El monto solicitado no puede ser menor a 1"); return;}

    this.MonRetStr = this.MonRet.toString();
    this.MonRetStr = this.MonRetStr.replace("$","");
    this.MonRetStr = this.MonRetStr.replace(",",".");
    this.MonRet = parseFloat(this.MonRetStr);

    this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Descripcion,Monto,FecHorReg",this.servicios.UsuMat.NRegistro+",|Retiro de fondos|,"+this.MonRet+",NOW()","","solret","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      this.servicios.EnvMsgToa("La solicitud fue registrada");
      this.locationX.back();

      this.servicios.AccSobBDAA("INSERT","No","NRegUsu,Titulo,Contenido,Accion,Datos,FecHor","1,|Nuevo Retiro|,|Nuevo retiro fue solicitado|,|Billetera|,|"+Res.lastinsertid+"|,NOW()","","noti","","","",this.MosLoa).then((dataRes)=>{
        let ResD: any = dataRes;
        //console.log(ResD);
      }).catch((err)=>{console.log(err)});
    }).catch((err)=>{console.log(err)});
  }
}
