import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SolretPage } from './solret.page';

const routes: Routes = [
  {
    path: '',
    component: SolretPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SolretPageRoutingModule {}
