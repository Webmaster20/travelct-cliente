import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-registro1',
  templateUrl: './registro1.page.html',
  styleUrls: ['./registro1.page.scss'],
})
export class Registro1Page implements OnInit {

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.servicios.VigGPS();
  }

  ConReg(){
    if (!this.servicios.ValCam("Número Móvil","Telefono",this.servicios.UsuReg.NumMov,"Si")){return;}

    this.servicios.UsuReg.CV = this.servicios.GenCod4Dig();
    if (this.servicios.DebMod){
      this.servicios.EnvMsgSim("Código","Tu código es: "+this.servicios.UsuReg.CV);
    }else{
      this.servicios.EnvSMS(this.servicios.UsuReg.NumMov,"Tu código "+this.servicios.NomApp+" es: "+this.servicios.UsuReg.CV);
    }
    this.router.navigate(["registro2"]);
  }
}
