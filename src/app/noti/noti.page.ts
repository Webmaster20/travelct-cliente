import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-noti',
  templateUrl: './noti.page.html',
  styleUrls: ['./noti.page.scss'],
})
export class NotiPage implements OnInit {

  LisNot: any;

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarLisNot()
  }

  MosDet(Not){
    switch (Not.Accion) {
      case "Solicitudes":
        this.MueSol(Not.Datos);
      break;

      case "Conductores":
        switch (Not.Titulo) {
          case "Nuevo Conductor":
            this.MueCon(Not.Datos);
          break;

          case "Alerta de Falla":
            this.MueCha(Not.Datos);
          break;
        }
      break;

      case "Chat":
        this.MueCha(Not.Datos);
      break;

      case "Panico":
        this.MueCha(Not.Datos);
      break;

      case "Soporte":
        this.MueSop(Not.Datos);
      break;
    }
  }

  MueSol(NRegSol){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegCli) NomApe,(SELECT Puntos FROM usuarios WHERE NRegistro = A.NRegCli) Puntos,(SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegCli) FotPer, (SELECT Nombre FROM especialidades WHERE NRegistro = A.NRegEsp) NomEsp, (SELECT Clave FROM especialidades WHERE NRegistro = A.NRegEsp) ClaEsp, (SELECT Nombre FROM servicios WHERE NRegistro = A.NRegSer) NomSer, (SELECT COUNT(*) FROM solpro WHERE NRegSol = A.NRegistro) ProRep","","","solicitudes A","WHERE NRegistro="+NRegSol+"","ORDER BY FecHorSol DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      let data: NavigationExtras = {state:{Sol:Res.data[0]}};
      this.router.navigate(["solicitudesdet"],data);
    }).catch((err)=>{console.log(err)});
  }

  MueCon(NRegCon){
    let data: NavigationExtras = {state:{PesAct:"Nuevos"}};
    this.router.navigate(["conductores"],data);
  }

  MueCha(NRegSol){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegCho) NomApe,(SELECT Puntos FROM usuarios WHERE NRegistro = A.NRegCho) Puntos,(SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegCho) FotPer","","","viajes A","WHERE NRegistro = "+NRegSol+"","ORDER BY FecHorSol DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      let data: NavigationExtras = {state:{Ped:Res.data[0]}};
      this.router.navigate(["chat"],data);
    }).catch((err)=>{console.log(err)});
  }

  MueSop(NRegCon){
    console.log(NRegCon);
  }

  CarLisNot(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","noti","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Estatus IN (|Nueva|,|Vista|)","ORDER BY FecHor DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisNot = Res.data;
    }).catch((err)=>{console.log(err)});
  }
}
