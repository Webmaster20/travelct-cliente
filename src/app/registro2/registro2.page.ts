import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-registro2',
  templateUrl: './registro2.page.html',
  styleUrls: ['./registro2.page.scss'],
})
export class Registro2Page implements OnInit {

  @ViewChild("ICV1") VICV1;
  @ViewChild("ICV2") VICV2;
  @ViewChild("ICV3") VICV3;
  @ViewChild("ICV4") VICV4;

  D1: string = "";
  D2: string = "";
  D3: string = "";
  D4: string = "";

  CVI: string = "";

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.VICV1.setFocus();
  }

  PasaIVC2(event){
    if (event.inputType != "deleteContentBackward"){
      this.VICV2.setFocus();
    }
  }
  PasaIVC3(event){
    if (event.inputType == "deleteContentBackward"){
      this.VICV1.setFocus();
    }else{
      this.VICV3.setFocus();
    }
  }
  PasaIVC4(event){
    if (event.inputType == "deleteContentBackward"){
      this.VICV2.setFocus();
    }else{
      this.VICV4.setFocus();
    }
  }
  PasaIVCX(event){
    if (event.inputType == "deleteContentBackward"){
      this.VICV3.setFocus();
    }
  }

  ConCod(){
    this.CVI = this.D1+" "+this.D2+" "+this.D3+" "+this.D4;
    if (this.servicios.UsuReg.CV != this.CVI){
      this.servicios.EnvMsgSim("Registro","El codigo ingresado no es correcto");
      return;
    }else{
      this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE Telefono = |"+this.servicios.UsuReg.NumMov+"| AND Tipo = |Cliente|","","",this.MosLoa).then((dataRes)=>{
        let Res: any = dataRes;
        //console.log(Res);
        if (Res.data.length > 0){
          this.servicios.AccSobBDAA("LOGINCLI","Si","",Res.data[0].Email+","+Res.data[0].Password+",","","","","","",this.MosLoa).then((dataRes)=>{
            let Res: any = dataRes;
            //console.log(Res);
     
            if (Res.data.length > 0){
              window.localStorage.setItem("Token",Res.data[0].Token);
              switch(Res.data[0].Estatus){
                case "Activo":
                  this.servicios.UsuMat = Res.data[0];
                  this.router.navigate(["home"]);
                break;
      
                case "Nuevo":
                  this.servicios.EnvMsgSim("Acceso","Usuario Nuevo, debe esperar la activación");
                  this.router.navigate(["login"]);
                break;
                
                case "Bloqueado":
                  this.servicios.EnvMsgSim("Acceso","Usuario Bloqueado, por favor contacte a soporte para mas detalles");
                  this.router.navigate(["login"]);
                break;
              }
            }
          }).catch((err)=>{console.log(err)});
        }else{
          this.router.navigate(["registro3"]);
        }
      }).catch((err)=>{console.log(err)});
    }
  }

  VerAyuda(){
    this.router.navigate(["ayuda"]);
  }

  Volver(){
    
  }
}
