import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-retirar',
  templateUrl: './retirar.page.html',
  styleUrls: ['./retirar.page.scss'],
})
export class RetirarPage implements OnInit {

  Trans: any;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarCue();
    this.CarTra();
  }

  SolRet(){
    this.router.navigate(["solret"]);
  }

  DatBan(){
    this.router.navigate(["datban"]);
  }

  CarTra(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","solret","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro,"ORDER BY FecHorReg DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.Trans = Res.data;
    }).catch((err)=>{console.log(err)});
  }

  CarCue(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","datcue","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro,"ORDER BY Banco","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      if (Res.data.length == 0){
        this.router.navigate(["datban"]);
        this.servicios.EnvMsgSim("Retiros","Por favor complete los datos bancarios");
      }
    }).catch((err)=>{console.log(err)});
  }
}