import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-billetera',
  templateUrl: './billetera.page.html',
  styleUrls: ['./billetera.page.scss'],
})
export class BilleteraPage implements OnInit {

  Trans: any;

  TotCob: any;
  TotPag: any;
  TotAbo: any;
  TotRet: any;

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.ActSal();
    this.CarTra();
    this.CalTotCob();
    this.CalTotPag();
    this.CalTotAbo();
    this.CalTotRet();
  }

  Retirar(){
    this.router.navigate(["retirar"]);
  }
  Abonar(){
    this.router.navigate(["abonar"]);
  }

  CarTra(){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegUsu) NomUsu, (SELECT Tipo FROM usuarios WHERE NRegistro = A.NRegUsu) TipUsu","","","billetera A","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro,"ORDER BY FecHor DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.Trans = Res.data;
    }).catch((err)=>{console.log(err)});
  }

  CalTotCob(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotCob","","","billetera","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Transaccion LIKE |Cobro·|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotCob = Res.data[0].TotCob;
    }).catch((err)=>{console.log(err)});
  }

  CalTotPag(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotPag","","","billetera","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Transaccion LIKE |Pago·|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotPag = Res.data[0].TotPag;
    }).catch((err)=>{console.log(err)});
  }

  CalTotAbo(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotAbo","","","billetera","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Transaccion LIKE |Abono·|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotAbo = Res.data[0].TotAbo;
    }).catch((err)=>{console.log(err)});
  }

  CalTotRet(){
    this.servicios.AccSobBDAA("SELECT","Si","SUM(Monto) TotRet","","","billetera","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro+" AND Transaccion LIKE |Retiro·|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.TotRet = Res.data[0].TotRet;
    }).catch((err)=>{console.log(err)});
  }

  ActSal(){
    this.servicios.AccSobBDAA("SELECT","Si","Saldo","","","usuarios","WHERE NRegistro="+this.servicios.UsuMat.NRegistro,"","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.UsuMat.Saldo = Res.data[0].Saldo;
    }).catch((err)=>{console.log(err)});
  }
}
