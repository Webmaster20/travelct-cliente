import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams, Platform } from '@ionic/angular';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-modpag',
  templateUrl: './modpag.page.html',
  styleUrls: ['./modpag.page.scss'],
})
export class ModpagPage implements OnInit {

  @ViewChild('InpFil') InpFil: ElementRef;

  TipMod: string = "";

  Titulo: string = ""
  Carpeta: string = "";
  LisIte: any;
  MosLoa: boolean = true;

  ValBus: string = "";

  DatLog: any = {"Ema":"","Pas":"","Rep":""};
  MosPas: string = "No";

  MedDom: any = {
    "Tar":"",
    "Met":""
  }

  ProSel: any = {
    "NReg": 0,
    "Des": "",
    "OtrDes":""
  }

  PacSel: string = "";

  Doc: any;
  Fil: any;
  CarFil: string = "No";

  Data: any;

  Sal: any;

  NRegEsp: any;
  MSR: any;
  
  Pre: any;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    public servicios: Servicios,
  ) {
    this.TipMod = this.navParams.data.TipMod;
    this.Doc = this.navParams.data.Doc;
    this.Data = this.navParams.data.Data;

    this.Sal = this.navParams.data.Sal;

    this.NRegEsp = this.navParams.data.NRegEsp;
    this.MSR = this.navParams.data.MSR;
    
    this.Pre = this.navParams.data.Pre;
  }

  ngOnInit() {
  }

  ionViewDidEnter(){
    switch (this.TipMod) {
      case "UplDoc":
        this.Titulo = "Enviar "+this.Doc.Descripcion;
        console.log(this.Doc);
      break;

      case "RepNue":
        this.Titulo = "Nuevo Repartidor";
        this.Carpeta = "usu";
      break;
      case "RutAct":
        this.Titulo = "Seleccione una Ruta";
        this.CarLisIte()
      break;
      case "SelCon":
        this.Titulo = "Seleccione un Conductor";
        this.Carpeta = "usu";
        this.CarLisIte()
      break;
      case "VerChe":
        this.Titulo = "Puntos de Chequeo";
        this.Carpeta = "";
        this.CarLisIte()
      break;
      case "VerRec":
        this.Titulo = "Reporte de Problemas";
        this.Carpeta = "";
        this.CarLisIte()
      break;

      case "EspCli":
        this.Titulo = "Seleccione las Especialidades";
        this.Carpeta = "cat";
        this.CarLisIte()
      break;
      case "LisCat":
        this.Titulo = "Seleccione las Categorías";
        this.Carpeta = "cat";
        this.CarLisIte()
      break;
      case "Cat":
        this.Titulo = "Seleccione una Categoría";
        this.Carpeta = "cat";
        this.CarLisIte()
      break;
      case "Esp":
        this.Titulo = "Seleccione una Especialidad";
        this.Carpeta = "esp";
        this.CarLisIte()
      break;
      case "EspMed":
        this.Titulo = "Seleccione una Especialidad";
        this.CarLisIte()
      break;

      case "Con":
        this.Titulo = "Cambiar contraseña";
        this.Carpeta = "";
      break;

      case "DomTar":
        this.Titulo = "Tarifa para el Domicilio";
        this.Carpeta = "";
      break;

      case "VerOfe":
        this.Titulo = "Ofertas para el Domicilio";
        this.Carpeta = "";
      break;

      case "PagVid":
        this.Titulo = "Pago de Video Consulta";
        this.Carpeta = "";
      break;

      case "ProVid":
        this.Titulo = "¿Problemas con la Video Consulta?";
        this.Carpeta = "";
        this.CarLisIte();
      break;

      case "CanSol":
        this.Titulo = "¿Cancelar el Viaje?";
        this.Carpeta = "";
        this.CarLisIte();
      break;

      case "CitPac":
        this.Titulo = "Seleccione el paciente";
        this.Carpeta = "";
        this.CarLisIte();
      break;

      case "PagSer":
        this.Titulo = "Pago de Servicio";
        this.Carpeta = "";
      break;

      case "EncVia":
        this.Titulo = "Encuesta";
        this.Carpeta = "";
      break;
    }
  }

  CarLisIte(){
    switch (this.TipMod) {
      case "RutAct":
        this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT COUNT(*) FROM paradas WHERE NRegRut=A.NRegistro) TotPar,(SELECT COUNT(*) FROM horarios WHERE NRegRut=A.NRegistro) TotHor","","","rutas A","WHERE NRegEmp = "+this.servicios.UsuMat.NRegistro+" AND Nombre LIKE |·"+this.ValBus+"·| AND Estatus=|Activa|","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "SelCon":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE Tipo=|Conductor| AND NRegEmp = "+this.servicios.UsuMat.NRegistro+" AND Nombre LIKE |·"+this.ValBus+"·| AND EstCon=|Conectado| AND NRegistro NOT IN (SELECT NRegCon FROM salidas WHERE Estatus IN (|Asignada|,|Aceptada|,|Iniciada|) AND YEAR(NOW())=YEAR(FecHorReg) AND MONTH(NOW())=MONTH(FecHorReg) AND DAY(NOW())=DAY(FecHorReg))","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "VerChe":
        console.log(this.Sal)
        this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT Nombre FROM paradas WHERE NRegistro=A.NRegPar) NomPar","","","chequeos A","WHERE NRegSal = "+this.Sal.NRegSal+"","ORDER BY FecHorChe","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "VerRec":
        console.log(this.Sal)
        this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro=A.NRegUsu) NomUsu","","","reclamos A","WHERE NRegSal = "+this.Sal.NRegSal+"","ORDER BY FecHorReg","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;


      case "EspCli":
        this.servicios.AccSobBDAA("SELECT","Si","*,(|No|) SerSel,(0) NMSR","","","servicios","WHERE NRegEsp = "+this.NRegEsp+"","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;

          for (let n = 0; n < this.MSR.length; n++) {
            for (let n2 = 0; n2 < this.LisIte.length; n2++) {
              if (this.LisIte[n2].NRegistro == this.MSR[n].NRegSer){
                this.LisIte[n2].NMSR = this.MSR[n].NMSR;
                this.LisIte[n2].SerSel = "Si";
              }
            }
          }
        }).catch((err)=>{console.log(err)});
      break;
      case "LisCat":
        let NRUA = "";
        if (this.servicios.UsuMat){
          NRUA = " AND NRegistro NOT IN (SELECT NRegCat FROM procat WHERE NRegPro = "+this.servicios.UsuMat.NRegistro+")"
        }
        this.servicios.AccSobBDAA("SELECT","Si","*,(|No|) CatSel","","","categorias","WHERE Nombre LIKE |·"+this.ValBus+"·| AND Estatus=|Activa| "+NRUA+"","ORDER BY NRegistro","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "Cat":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","categorias","WHERE Nombre LIKE |·"+this.ValBus+"·| AND Estatus=|Activa|","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "Esp":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","especialidades","WHERE NRegCat="+this.servicios.UsuMat.NRegCat+" AND Nombre LIKE |·"+this.ValBus+"·| AND Estatus=|Activa|","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "EspMed":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","espmed","WHERE Nombre LIKE |·"+this.ValBus+"·| AND Estatus=|Activa|","ORDER BY Nombre","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
      case "ProVid":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","defpro","WHERE NRegEsp = 3 AND TipUsu=|Profesional| AND Estatus=|Activa|","ORDER BY Descripcion","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;

      case "CanSol":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","defpro","WHERE TipUsu=|Cliente| AND Estatus=|Activa|","ORDER BY Descripcion","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;

      case "CitPac":
        this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE NRegistro = "+this.servicios.UsuMat.NRegistro+" OR NRegCli = "+this.servicios.UsuMat.NRegistro,"ORDER BY Nombre,Apellido","",this.MosLoa).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
          this.LisIte = Res.data;
        }).catch((err)=>{console.log(err)});
      break;
    }
  }

  ActCar(){
    switch (this.TipMod) {
      case "RutAct":
        this.CarLisIte();
      break;
      case "SelCon":
        this.CarLisIte();
      break;

      case "Esp":
        this.CarLisIte();
      break;
      case "EspMed":
        this.CarLisIte();
      break;
      case "Cat":
        this.CarLisIte();
      break;
      case "LisCat":
        this.CarLisIte();
      break;
    }
  }

  SelArc(){
    this.InpFil.nativeElement.click();
  }
  LoaFilSel(Eve){
    this.CarFil = "Si"
    //console.log(Eve);
    this.Fil = Eve.target.files[0];

    this.servicios.UplFil(this.Fil).then((Res)=>{
      //console.log(Res);
      this.CarFil = "No"
      this.closeModal(Res);
    }).catch((err)=>{
      //console.log(err)
      this.closeModal("Err");
    })

  }

  async SerDel(Ser){
    console.log("Eliminar el PROSER N "+Ser.NMSR)

    const alert = await this.servicios.alertController.create({
      //cssClass: 'my-custom-class',
      header: "Quitar Especialidad",message: "¿Esta seguro que desea Quitar la Especialidad "+Ser.Nombre+" de sus ofertas?",
      buttons: [
        {text: "Si",
          handler: () => {
            this.servicios.AccSobBDAA("DELETE","No","","","","proser","WHERE NRegistro = "+Ser.NMSR,"","",this.MosLoa).then((dataRes)=>{
              let Res: any = dataRes;
              //console.log(Res);
              Ser.NMSR = 0;
              Ser.SerSel = "No";
            }).catch((err)=>{console.log(err)});
          }},
        {text: "No",role: "cancel",handler: () => {}}
      ]
    });
    await alert.present();
  }
  SerSel(Ser){
    if (Ser.SerSel == "No"){
      Ser.SerSel = "Si";
    }else{
      Ser.SerSel = "No";
    }
  }
  ProEspCli(){
    let Sel: boolean = false;
    let ObjSerSel: any = []
    for (let n = 0; n < this.LisIte.length; n++) {
      if (this.LisIte[n].SerSel == "Si"){
        Sel = true;
        ObjSerSel.push(this.LisIte[n]);
      }
    }
    if (!Sel){this.servicios.EnvMsgSim(this.servicios.NomApp,"Debe seleccionar al menos una Especialidad"); return;}
    
    this.closeModal(ObjSerSel);
  }

  CatSel(Cat){
    if (Cat.CatSel == "No"){
      Cat.CatSel = "Si";
    }else{
      Cat.CatSel = "No";
    }
  }
  ProLisCat(){
    let Sel: boolean = false;
    let ObjCatSel: any = []
    for (let n = 0; n < this.LisIte.length; n++) {
      if (this.LisIte[n].CatSel == "Si"){
        Sel = true;
        ObjCatSel.push(this.LisIte[n]);
      }
    }
    if (!Sel){this.servicios.EnvMsgSim(this.servicios.NomApp,"Debe seleccionar al menos una Categoría"); return;}
    
    this.closeModal(ObjCatSel);
  }

  ProVidSel(Pro){
    console.log(Pro);
    this.ProSel.NReg = Pro.NRegistro;
    this.ProSel.Des = Pro.Descripcion;
  }

  ConPro(){
    if (this.ProSel.NReg == 0){this.servicios.EnvMsgSim(this.servicios.NomApp,"Debe indicar un problema a reportar"); return;}
    if (this.ProSel.Des == "Otro" && this.ProSel.OtrDes == ""){this.servicios.EnvMsgSim(this.servicios.NomApp,"Debe describir el problema a reportar"); return;}

    this.closeModal(this.ProSel);
  }

  CamCon(){
    if (!this.servicios.ValCam("Nueva Contraseña","Texto",this.DatLog.Pas,"Si")){return;}
    if (!this.servicios.ValCam("Repita Contraseña","Texto",this.DatLog.Rep,"Si")){return;}
    if (this.DatLog.Pas != this.DatLog.Rep){this.servicios.EnvMsgSim("Acceso","Las contraseñas no coinciden");return;}

    this.servicios.AccSobBDAA("UPDATE","No","","","Password = |"+this.DatLog.Pas+"|","usuarios","WHERE NRegistro=|"+this.servicios.UsuMat.NRegistro+"|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.closeModal("");
      this.servicios.EnvMsgToa("La contraseña fue cambiada");
    }).catch((err)=>{console.log(err)});
  }

  MosOcuCon(input: any,input2: any): any {
    input.type = input.type === 'password' ?  'text' : 'password';
    input.selectionStart = 999;
    input.selectionEnd = 999;

    input2.type = input2.type === 'password' ?  'text' : 'password';
    input2.selectionStart = 999;
    input2.selectionEnd = 999;

    this.MosPas = this.MosPas == "No" ? "Si" : "No";
  }

  OfrTar(){
    if (!this.servicios.ValCam("Tarifa","Decimal",this.MedDom.Tar,"Si")){return;}
    if (!this.servicios.ValCam("Método","Texto",this.MedDom.Met,"Si")){return;}

    this.closeModal(this.MedDom);
  }

  async closeModal(NRegIte) {
    const ResMod: string = NRegIte;
    await this.modalController.dismiss(ResMod);
  }
}
