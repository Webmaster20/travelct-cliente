import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModpagPage } from './modpag.page';

const routes: Routes = [
  {
    path: '',
    component: ModpagPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModpagPageRoutingModule {}
