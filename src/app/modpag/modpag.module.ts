import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModpagPageRoutingModule } from './modpag-routing.module';

import { ModpagPage } from './modpag.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModpagPageRoutingModule
  ],
  declarations: [ModpagPage]
})
export class ModpagPageModule {}
