import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-viajes',
  templateUrl: './viajes.page.html',
  styleUrls: ['./viajes.page.scss'],
})
export class ViajesPage implements OnInit {

  LisPP: any;

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarLisPP()
  }

  VerCha(Ped){
    let data: NavigationExtras = {state:{Ped:Ped}};
    this.router.navigate(["chat"],data);
  }

  CarLisPP(){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT CONCAT(Nombre,| |,Apellido) FROM usuarios WHERE NRegistro = A.NRegCho) NomApe,(SELECT Puntos FROM usuarios WHERE NRegistro = A.NRegCho) Puntos,(SELECT FotPer FROM usuarios WHERE NRegistro = A.NRegCho) FotPer","","","viajes A","WHERE NRegCli="+this.servicios.UsuMat.NRegistro+"","ORDER BY FecHorSol DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.LisPP = Res.data;
    }).catch((err)=>{console.log(err)});
  }

}