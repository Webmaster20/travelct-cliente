import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-datban',
  templateUrl: './datban.page.html',
  styleUrls: ['./datban.page.scss'],
})
export class DatbanPage implements OnInit {

  DatBan: any;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarDatBan();
  }

  AgrDat(){
    this.router.navigate(["datbanagr"]);
  }

  EliDat(Ban){
    this.servicios.AccSobBDAA("DELETE","No","","","","datcue","WHERE NRegistro="+Ban.NRegistro,"","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.CarDatBan();
    }).catch((err)=>{console.log(err)});
  }

  CarDatBan(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","datcue","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro,"ORDER BY Banco","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DatBan = Res.data;
    }).catch((err)=>{console.log(err)});
  }
}