import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatbanPageRoutingModule } from './datban-routing.module';

import { DatbanPage } from './datban.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatbanPageRoutingModule
  ],
  declarations: [DatbanPage]
})
export class DatbanPageModule {}
