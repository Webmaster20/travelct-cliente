import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatbanPage } from './datban.page';

const routes: Routes = [
  {
    path: '',
    component: DatbanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatbanPageRoutingModule {}
