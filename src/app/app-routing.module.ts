import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'ini',
    pathMatch: 'full'
  },
  {
    path: 'ini',
    loadChildren: () => import('./ini/ini.module').then( m => m.IniPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro1',
    loadChildren: () => import('./registro1/registro1.module').then( m => m.Registro1PageModule)
  },
  {
    path: 'registro2',
    loadChildren: () => import('./registro2/registro2.module').then( m => m.Registro2PageModule)
  },
  {
    path: 'registro3',
    loadChildren: () => import('./registro3/registro3.module').then( m => m.Registro3PageModule)
  },
  {
    path: 'perfil',
    loadChildren: () => import('./perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'modpag',
    loadChildren: () => import('./modpag/modpag.module').then( m => m.ModpagPageModule)
  },
  {
    path: 'ayuda',
    loadChildren: () => import('./ayuda/ayuda.module').then( m => m.AyudaPageModule)
  },
  {
    path: 'prefre1',
    loadChildren: () => import('./prefre1/prefre1.module').then( m => m.Prefre1PageModule)
  },
  {
    path: 'prefre2',
    loadChildren: () => import('./prefre2/prefre2.module').then( m => m.Prefre2PageModule)
  },
  {
    path: 'noti',
    loadChildren: () => import('./noti/noti.module').then( m => m.NotiPageModule)
  },
  {
    path: 'mensajes',
    loadChildren: () => import('./mensajes/mensajes.module').then( m => m.MensajesPageModule)
  },
  {
    path: 'mapbusjav',
    loadChildren: () => import('./mapbusjav/mapbusjav.module').then( m => m.MapbusjavPageModule)
  },
  {
    path: 'chat',
    loadChildren: () => import('./chat/chat.module').then( m => m.ChatPageModule)
  },
  {
    path: 'meneme',
    loadChildren: () => import('./meneme/meneme.module').then( m => m.MenemePageModule)
  },
  {
    path: 'solser',
    loadChildren: () => import('./solser/solser.module').then( m => m.SolserPageModule)
  },
  {
    path: 'viajes',
    loadChildren: () => import('./viajes/viajes.module').then( m => m.ViajesPageModule)
  },
  {
    path: 'billetera',
    loadChildren: () => import('./billetera/billetera.module').then( m => m.BilleteraPageModule)
  },
  {
    path: 'retirar',
    loadChildren: () => import('./retirar/retirar.module').then( m => m.RetirarPageModule)
  },
  {
    path: 'abonar',
    loadChildren: () => import('./abonar/abonar.module').then( m => m.AbonarPageModule)
  },
  {
    path: 'solabo',
    loadChildren: () => import('./solabo/solabo.module').then( m => m.SolaboPageModule)
  },
  {
    path: 'solret',
    loadChildren: () => import('./solret/solret.module').then( m => m.SolretPageModule)
  },
  {
    path: 'datban',
    loadChildren: () => import('./datban/datban.module').then( m => m.DatbanPageModule)
  },
  {
    path: 'datbanagr',
    loadChildren: () => import('./datbanagr/datbanagr.module').then( m => m.DatbanagrPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
