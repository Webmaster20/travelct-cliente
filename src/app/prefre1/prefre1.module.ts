import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Prefre1PageRoutingModule } from './prefre1-routing.module';

import { Prefre1Page } from './prefre1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Prefre1PageRoutingModule
  ],
  declarations: [Prefre1Page]
})
export class Prefre1PageModule {}
