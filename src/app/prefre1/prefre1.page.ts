import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-prefre1',
  templateUrl: './prefre1.page.html',
  styleUrls: ['./prefre1.page.scss'],
})
export class Prefre1Page implements OnInit {

  LisTem: any;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarLisTem()
  }

  VerPre(Tem){
    let data: NavigationExtras = {state:{Tem:Tem}};
    this.router.navigate(["prefre2"],data);
  }

  CarLisTem(){
    this.servicios.AccSobBDAA("SELECT","Si","DISTINCT(Tema)","","","prefre","WHERE Usuario=|"+this.servicios.UsuMat.Tipo+"| AND Estatus=|Activa|","ORDER BY Tema","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      this.LisTem = Res.data;
    }).catch((err)=>{console.log(err)});
  }
}
