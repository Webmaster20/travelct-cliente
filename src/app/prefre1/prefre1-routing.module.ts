import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Prefre1Page } from './prefre1.page';

const routes: Routes = [
  {
    path: '',
    component: Prefre1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Prefre1PageRoutingModule {}
