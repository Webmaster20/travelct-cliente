import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AbonarPageRoutingModule } from './abonar-routing.module';

import { AbonarPage } from './abonar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AbonarPageRoutingModule
  ],
  declarations: [AbonarPage]
})
export class AbonarPageModule {}
