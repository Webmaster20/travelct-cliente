import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-abonar',
  templateUrl: './abonar.page.html',
  styleUrls: ['./abonar.page.scss'],
})
export class AbonarPage implements OnInit {

  Trans: any;
  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios,
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.CarTra();
  }

  SolAbo(){
    this.router.navigate(["solabo"]);
  }

  CarTra(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","solabo","WHERE NRegUsu="+this.servicios.UsuMat.NRegistro,"ORDER BY FecHorReg DESC","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.Trans = Res.data;
    }).catch((err)=>{console.log(err)});
  }
}