import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { IonContent, ModalController } from '@ionic/angular';
import { ModpagPage } from '../modpag/modpag.page';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  @ViewChild(IonContent) content: IonContent

  Mensajes: any;
  NueMen: string = "";
  NueFot: string = "";
  NumMenAct: number = 0;

  DetFot: any = {
    "Carpeta":"cha",
    "Destino":"Chat"
  }
  CarImg: string = "";

  PedAct: any;
  DetVen: any;
  DetCli: any;

  RNM: any;

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public route: ActivatedRoute,
    public servicios: Servicios,
    public modalController: ModalController
  ){
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.PedAct = this.router.getCurrentNavigation().extras.state.Ped;
        this.CarDetCho();
        this.CarDetCli();
        this.CarMenCha();
      }
    });
  }

  ngOnInit(){
  }

  ionViewDidEnter(){
    this.CarMenCha();
    this.RNM = setInterval(() => {this.CarMenCha()},3000);
    while(this.servicios.DatNot.Chat.length > 0){this.servicios.DatNot.Chat.pop();};
  }
  ionViewDidLeave(){
    clearInterval(this.RNM);
    while(this.servicios.DatNot.Chat.length > 0){this.servicios.DatNot.Chat.pop();};
  }


  EnvMen(){
    let Campos = "NRegPed,NRegUsu,Mensaje,Imagen,FecHorReg";
    let Valores = "|"+this.PedAct.NRegistro+"|,|"+this.servicios.UsuMat.NRegistro+"|,|"+this.NueMen+"|,|"+this.NueFot+"|,NOW()"
    this.servicios.AccSobBDAA("INSERT","No",Campos,Valores,"","chat","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      console.log(Res);
      this.CarMenCha();

      let Campos = "";
      let Valores = "";
      Campos = "NRegUsu,Titulo,Contenido,Accion,Datos,FecHor";
      Valores = this.DetVen.NRegistro+",|Nuevo mensaje|,|"+this.NueMen+"|,|Chat|,|"+this.PedAct.NRegistro+"|,NOW()"
      this.servicios.AccSobBDAA("INSERT","No",Campos,Valores,"","noti","","","",this.MosLoa).then((dataRes)=>{
        let Res: any = dataRes;
        console.log(Res);
        this.servicios.EnvNotPus(this.DetVen.NRegistro,"Nuevo mensaje",this.NueMen,"Chat",this.PedAct.NRegistro).then((dataRes)=>{
          let ResE: any = dataRes;
          //console.log(ResE);
        }).catch((err)=>{console.log(err)});
      }).catch((err)=>{console.log(err)});

      this.NueMen = "";
      this.NueFot = "";

    }).catch((err)=>{console.log(err)});
    while(this.servicios.DatNot.Chat.length > 0){this.servicios.DatNot.Chat.pop();};
  }

  async OpeModZoo(Img,Tex){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {"TipMod": "ZooCha","DatMen":{"Img":Img,"Tex":Tex}}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null){
        //console.log(this.ModRes);
      }
    });
    return await modal.present();
  }

  SelFot(){
    this.servicios.SelFot(this);
  }

  MueUltMen(){
    setTimeout(() => {this.content.scrollToBottom(200)},5);
  }

  CarDetCho(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE NRegistro="+this.PedAct.NRegCho,"","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DetVen = Res.data[0];
    }).catch((err)=>{console.log(err)});
  }

  CarDetCli(){
    this.servicios.AccSobBDAA("SELECT","Si","*","","","usuarios","WHERE NRegistro="+this.PedAct.NRegCli,"","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.DetCli = Res.data[0];
    }).catch((err)=>{console.log(err)});
  }

  CarMenCha(){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT Nombre FROM usuarios WHERE NRegistro=A.NRegUsu) NomUsu, (SELECT Tipo FROM usuarios WHERE NRegistro=A.NRegUsu) TipUsu, (SELECT FotPer FROM usuarios WHERE NRegistro=A.NRegUsu) FotPer","","","chat A","WHERE NRegPed="+this.PedAct.NRegistro,"ORDER BY FecHorReg","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      if (this.NumMenAct != Res.data.length){
        this.Mensajes = Res.data;
        this.NumMenAct = Res.data.length
        this.MueUltMen();
      }else if (Res.data.length == 0){
        this.Mensajes = [];
      }
    }).catch((err)=>{console.log(err)});

    for (let n = 0; n < this.servicios.DatNot.Chat.length; n++) {
      if (this.servicios.DatNot.Chat[n].Datos = this.PedAct.NRegistro){
        this.servicios.DatNot.Chat.splice(n,1);
      }
    }
  }
}