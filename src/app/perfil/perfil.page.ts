import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Servicios } from 'src/servicios/servicios';
import { ModalController } from '@ionic/angular';
import { ModpagPage } from '../modpag/modpag.page';

//import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  LisCat: any;

  DetFot: any = {
    "Carpeta":"usu",
    "Destino":"Perfil"
  }

  NRegEsp: number = 0;
  DesEsp: string = "";

  MosLoa: boolean = true;
  ModRes: any;

  constructor(
    public router: Router,
    public servicios: Servicios,
    public modalController: ModalController,
    //public socialSharing: SocialSharing
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.servicios.AccSobBDAA("SELECT","Si","*,(SELECT Nombre FROM EspMed WHERE NRegistro=A.NRegEsp) DesEsp","","","proesp A","WHERE NRegPro = "+this.servicios.UsuMat.NRegistro+"","ORDER BY NRegistro LIMIT 1","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      if (Res.data.length > 0){
        this.NRegEsp = Res.data[0].NRegistro;
        this.DesEsp = Res.data[0].DesEsp;
      }else{
        this.NRegEsp = 0;
        this.DesEsp = "Seleccione ..."
      }
    }).catch((err)=>{console.log(err)});
  }

  /*
  ComApiKey(){
    this.socialSharing.shareViaEmail("APIKey: "+this.servicios.UsuMat.APIKey,"ApiKey Galiverys Vendedor "+this.servicios.UsuMat.Nombre,this.servicios.UsuMat.Email).then(()=>{
      this.servicios.EnvMsgSim("APIKey Enviado","El APIKey fue enviado a su correo electrónico")
    }).catch((error) => {
      this.servicios.EnvMsgSim("Error al enviar","Hubo un problema al enviar el correo")
    });
  }
  */

  Guardar(){
    if (!this.servicios.ValCam("Nombre","Texto",this.servicios.UsuMat.Nombre,"Si")){return;}
    if (!this.servicios.ValCam("Apellido","Texto",this.servicios.UsuMat.Apellido,"Si")){return;}
    //if (!this.servicios.ValCam("Identificación","Texto",this.servicios.UsuMat.Identificacion,"Si")){return;}
    if (!this.servicios.ValCam("Correo electrónico","Email",this.servicios.UsuMat.Email,"Si")){return;}

    if (this.servicios.Origen.OriDes != ""){
      this.servicios.UsuMat.UbiEmp = this.servicios.Origen.Origen;
      this.servicios.UsuMat.UbiEmpDes = this.servicios.Origen.OriDes;
    }

    if (!this.servicios.ValCam("Ubicación de la Cliente","Texto",this.servicios.UsuMat.UbiEmpDes,"Si")){return;}

    this.servicios.AccSobBDAA("UPDATE","No","","","Nombre=|"+this.servicios.UsuMat.Nombre+"|,Apellido=|"+this.servicios.UsuMat.Apellido+"|,Identificacion=|"+this.servicios.UsuMat.Identificacion+"|,Email=|"+this.servicios.UsuMat.Email+"|,UbiEmp=|"+this.servicios.UsuMat.UbiEmp+"|,UbiEmpDes=|"+this.servicios.UsuMat.UbiEmpDes+"|,FotPer=|"+this.servicios.UsuMat.FotPer+"|","usuarios","WHERE NRegistro=|"+this.servicios.UsuMat.NRegistro+"|","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);
      this.servicios.EnvMsgToa("Los cambios fueron guardados");
    }).catch((err)=>{console.log(err)});
  }

  async OpeModEsp(){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {"TipMod": "EspMed"}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null){
        this.ModRes = dataReturned.data;
        console.log(this.ModRes);
        if (this.ModRes){
          if (this.NRegEsp == 0){
            this.servicios.AccSobBDAA("INSERT","No","NRegPro,NRegEsp",this.servicios.UsuMat.NRegistro+","+this.ModRes.NRegistro,"","proesp","","","",this.MosLoa).then((dataRes)=>{
              let Res: any = dataRes;
              //console.log(Res);

              this.ionViewDidEnter();
            }).catch((err)=>{console.log(err)});
          }else{
            this.servicios.AccSobBDAA("UPDATE","No","","","NRegEsp="+this.ModRes.NRegistro,"proesp","WHERE NRegistro=|"+this.NRegEsp+"|","","",this.MosLoa).then((dataRes)=>{
              let Res: any = dataRes;
              //console.log(Res);

              this.ionViewDidEnter();
            }).catch((err)=>{console.log(err)});
          }
        }
      }
    });
    return await modal.present();
  }

  VerMap(OriDes){
    let data: NavigationExtras = {state:{OriDes:OriDes}};
    //this.router.navigate(["mapbus"],data);
    this.router.navigate(["mapbusjav"],data);
  }

  async OpeModCon(){
    const modal = await this.modalController.create({component: ModpagPage,componentProps: {"TipMod": "Con"}});
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null){
        this.ModRes = dataReturned.data;
        //console.log(this.ModRes);
      }
    });
    return await modal.present();
  }

  TomFot(){
    this.servicios.SelFot(this);
  }
}