import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuController, Platform } from '@ionic/angular';
import { Servicios } from 'src/servicios/servicios';
import { Router, NavigationExtras } from '@angular/router';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';

declare var cordova;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public servicios: Servicios,
    public router: Router,
    public menuCtrl: MenuController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      FCM.getToken().then((token: string) => {
        console.log("fcm-token: "+token);
        window.localStorage.setItem("TPN",token);
      }).catch(error => {
        console.log(error);
      });

      FCM.onTokenRefresh().subscribe((token: string) => {
        console.log("fcm-token / onTokenRefresh: "+token);
        window.localStorage.setItem("TPN",token);
      });

      FCM.onNotification().subscribe(data => {
        this.servicios.ODN = data
        if (data.wasTapped){
          //Cuando la aplicacion no esta activa
          console.log("App al fondo: "+JSON.stringify(data));
        }else{
          //Esto indica que la App esta en primer plano
          console.log("Primer Plano: "+JSON.stringify(data));
        }
      }, error =>{
        console.log(error)
      });

      window.localStorage.setItem("LN","");
      cordova.plugins.notification.local.on('click', function (obj) {
        window.localStorage.setItem("LN",obj.title);
      });

      this.splashScreen.hide();
    });
  }


  MenCli(Item){
    switch (Item){
      case "Perfil":
        this.router.navigate(["perfil"]);
      break;

      case "Viajes":
        this.router.navigate(["viajes"]);
        while(this.servicios.DatNot.Viajes.length > 0){this.servicios.DatNot.Viajes.pop();};
      break;

      case "Billetera":
        this.router.navigate(["billetera"]);
      break;

      case "Salidas":
        this.router.navigate(["salidas"]);
        while(this.servicios.DatNot.Salidas.length > 0){this.servicios.DatNot.Salidas.pop();};
      break;

      case "Conductores":
        this.router.navigate(["conductores"]);
        while(this.servicios.DatNot.Conductores.length > 0){this.servicios.DatNot.Conductores.pop();};
      break;

      case "Unidades":
        this.router.navigate(["unidades"]);
        //while(this.servicios.DatNot.Unidades.length > 0){this.servicios.DatNot.Unidades.pop();};
      break;

      case "Rutas":
        this.router.navigate(["rutas"]);
        //while(this.servicios.DatNot.Rutas.length > 0){this.servicios.DatNot.Rutas.pop();};
      break;

      case "Solicitudes":
        this.router.navigate(["solicitudes"]);
        while(this.servicios.DatNot.Solicitudes.length > 0){this.servicios.DatNot.Solicitudes.pop();};
      break;

      case "Servicios":
        this.router.navigate(["servicios"]);
        while(this.servicios.DatNot.Servicios.length > 0){this.servicios.DatNot.Servicios.pop();};
      break;

      case "Pacientes":
        this.router.navigate(["pacientes"]);
      break;

      case "Mensajes":
        this.router.navigate(["mensajes"]);
        while(this.servicios.DatNot.Chat.length > 0){this.servicios.DatNot.Chat.pop();};
      break;

      case "Noti":
        this.router.navigate(["noti"]);
        while(this.servicios.DatNot.Panico.length > 0){this.servicios.DatNot.Panico.pop();};
      break;
      
      case "Reclamos":
        this.router.navigate(["reclamos"]);
      break;

      case "Compartir":
        this.servicios.EnvMsgToa("Proceso en espera de publicacion de la App ...")
      break;

      case "Ayuda":
        this.router.navigate(["ayuda"]);
        while(this.servicios.DatNot.Soporte.length > 0){this.servicios.DatNot.Soporte.pop();};
      break;
    
      case "Salir":
        window.localStorage.setItem("Token","");
        this.router.navigate(["login"]);
      break;
      
    }
    this.menuCtrl.close();
  }
}
