import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Servicios } from 'src/servicios/servicios';

@Component({
  selector: 'app-ini',
  templateUrl: './ini.page.html',
  styleUrls: ['./ini.page.scss'],
})
export class IniPage implements OnInit {

  Token: string = "";

  MosLoa: boolean = true;

  constructor(
    public router: Router,
    public servicios: Servicios
  ){
  }

  ngOnInit(){
    //window.localStorage.setItem("Token","");
    this.Token = window.localStorage.getItem("Token");
    //console.log(this.Token+"***");
    if (this.Token != "" && this.Token != null && this.Token != undefined){
      this.Login();
    }else{
      this.router.navigate(["login"]);
    }
    this.servicios.VigGPS();
  }

  Login(){
    this.servicios.AccSobBDAA("LOGINCLI","Si","",",,"+this.Token,"","","","","",this.MosLoa).then((dataRes)=>{
      let Res: any = dataRes;
      //console.log(Res);

      if (Res.data.length > 0){
        window.localStorage.setItem("token",Res.data[0].Token);
        switch(Res.data[0].Estatus){
          case "Activo":
            this.servicios.UsuMat = Res.data[0];
            this.router.navigate(["home"]);
          break;

          case "Nuevo":
            this.router.navigate(["login"]);
          break;
          
          case "Bloqueado":
            this.router.navigate(["login"]);
          break;
        }
      }else{
        this.router.navigate(["login"]);
      }
    }).catch((err)=>{console.log(err)});
  }
}