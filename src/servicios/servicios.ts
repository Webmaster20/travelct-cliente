import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, LoadingController, ActionSheetController, ToastController } from '@ionic/angular';
import { Geolocation, Geoposition, PositionError } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import * as moment from 'moment';

declare var cordova: any;

@Injectable()
export class Servicios{
  DarMod: boolean;
  NomApp: string = "TravElct";
  IdeApp: string = "com.travelct.cliente";
  ApiUrl: string = "http://35.238.26.205/travelct/";
  EmaSop: string = "soporte@travelct.com";
  DebMod: boolean = true;

  Pais: any = {
    "Nombre":"Mexico",
    "Codigo":"+52",
    "Bandera":"banmex.png",
    "TerCon":"tercon.pdf",
    "SimMon":"$",
    "Lat":"19.433021004809355",
    "Lon":"-99.13516746685117"
  }

  LoaCon: any;
  dataRCV: any;

  UsuMat: any;
  LatLon: any = {
    "Com":"",
    "Lat":"",
    "Lon":"",
    "Vel":"",
    "Ori":""
  };
  lastImage: string = null;

  MapBus: string = "";
  Origen: any = {
    Origen:"",
    OriDes:"",
    OriZon:""
  }
  Destino: any = {
    Destino:"",
    DesDes:"",
    DesZon:""
  }
  Delivery: any = {
    Delivery:"",
    DesDel:"",
    DesZon:""
  }

  UsuReg: any = {
    "Nombre":"",
    "Apellido":"",
    "Representante":"",
    "ID":"",
    "Email":"",
    "Pas":"",
    "ConPas":"",
    "NumMov":"",
    "FecNac":"",
    "FotPer":"fpd.png",
    "Categoria":"",
    "DesCat":"",
    "LisCat":{},
    "CV":"",
    "LRS": "No"
  }

  ODN: any = {};

  DatNot: any = {
    "Viajes":[],
    "Salidas":[],
    "Solicitudes":[],
    "Servicios":[],
    "Conductores":[],
    "Chat":[],
    "Soporte":[],
    "Panico":[]
  }

  ParGen: any;

  DirAct: any = {
    "Des":"",
    "LatLon":""
  }

  SolVid: any = {
    SVCI: false,
    NRegPro: 0,
    Precio: 0,
    Descuento: 0,
    Base: 0,
    ForPag: ""
  }

  constructor(
    public http: HttpClient,
    public alertController: AlertController,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public actionSheetController: ActionSheetController,
    public geolocation: Geolocation,
    public camera: Camera,
    public file: File,
    public transfer: FileTransfer,
    public locnot: LocalNotifications
  ){}

  async AccSobBDAA(Acc,Res,Cam,Val,CyV,Tab,Don,Ord,Sen,MosLoa){
    if (MosLoa){this.MosLoaCon();}
    let JSONSend:any = {"acc":Acc,"res":Res,"cam":Cam,"val":Val,"cyv":CyV,"tab":Tab,"don":Don,"ord":Ord,"sen":Sen,"tok":window.localStorage.getItem("Token")};
    JSONSend = JSON.stringify(JSONSend);
    //console.log(JSONSend);

    let body:string = "jsonsend="+JSONSend;
    let headers:any = new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"});
    let options:any = ({headers:headers});
    let url:any = this.ApiUrl + "api/admbd.php";

    let dataRes = await this.http.post(url,body,options).toPromise();
    this.OcuLoaCon();
    //console.log(dataRes);
    return dataRes;
  }

  async CalTarVia(Distancia,Tiempo){
    let CLL: string = this.LatLon.Com;
    let OLL: string = this.Origen.Origen;
    let DLL: string = this.Destino.Destino;

    let DV: string = Distancia;
    let ME: string = Tiempo;

    let NV: string = "0";
    let CP: string = "1";

    let body:string = "CLL=" + CLL + "&OLL=" + OLL + "&DLL=" + DLL + "&DV=" + DV + "&ME=" + ME + "&NV=" + NV + "&DVPri=" + "&CP=" + CP;
    let headers:any = new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"});
    let options:any = ({headers:headers});
    let url:any = this.ApiUrl + "api/caltar.php";

    let dataRes = await this.http.post(url,body,options).toPromise();
    this.OcuLoaCon();
    //console.log(dataRes);
    return dataRes;
  }

  async EnvNotPus(NRegUsu,Tit,Men,Acc,Dat){
    let body:string = "NRegUsu=" + NRegUsu + "&Tit=" + Tit + "&Men=" + Men + "&Acc=" + Acc + "&Dat=" + Dat;
    let headers:any = new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"});
    let options:any = ({headers:headers});
    let url:any = this.ApiUrl + "api/sennotpus.php";

    let dataRes = await this.http.post(url,body,options).toPromise();
    //console.log(dataRes);
    return dataRes;
  }

  async OlvCla(Email){
    let body:string = "ema=" + Email;
    let headers:any = new HttpHeaders({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"});
    let options:any = ({headers:headers});
    let url:any = this.ApiUrl + "api/reccla.php";

    let dataRes = await this.http.post(url,body,options).toPromise();
    //console.log(dataRes);
    return dataRes;
  }

  VigGPS(){
    let watch = this.geolocation.watchPosition();
    watch.subscribe((data) => {
      let DGA = (data as Geoposition);
      this.LatLon.Com = DGA.coords.latitude+","+DGA.coords.longitude+","+DGA.coords.heading+","+DGA.coords.speed;
      this.LatLon.Lat = DGA.coords.latitude;
      this.LatLon.Lon = DGA.coords.longitude;
      this.LatLon.Ori = DGA.coords.heading;
      this.LatLon.Vel = DGA.coords.speed;

      if (this.UsuMat){
        this.AccSobBDAA("UPDATE","No","","","UbiAct=|"+this.LatLon.Lat+","+this.LatLon.Lon+"|","usuarios","WHERE NRegistro="+this.UsuMat.NRegistro,"","",false).then((dataRes)=>{
          let Res: any = dataRes;
          //console.log(Res);
        }).catch((err)=>{console.log(err)});
      }
    });
  }

  AjuFPRS(URL){
    let re = /\[\]/gi;
    URL = URL.replace(re,"&");
    return URL;
  }

  EnvLocNot(ID,Tit,Tex,Cla,Val){
    /*
    this.locnot.schedule({
      id: ID,
      title: Tit,
      text: Tex,
      vibrate: true,
      //sound: isAndroid? 'file://sound.mp3': 'file://beep.caf',
      data: {secret: Cla, value: Val}
    });
    */
  }

  EnvSMS(Num,Men){
  }

  ObtRelTimMom(Val,For){
    return moment(Val,For).locale("es").fromNow();
  }

  ObtFecComMom(Val,For){
    return moment(Val).locale("es").format(For);
  }

  ColEstUnd(Estatus){
    switch(Estatus){
      case "Activa":
        return "#32ad32";
      case "Disponible":
        return "#60cdd4";
      case "Taller":
        return "#f25513";
    }
  }

  ColEstRut(Estatus){
    switch(Estatus){
      case "Nueva":
        return "#60cdd4";
      case "Inactiva":
        return "#f25513";
      case "Activa":
        return "#32ad32";
      
    }
  }

  ColEstVen(Estatus){
    switch(Estatus){
      case "Desconectado":
        return "rgba(100,100,100,0.7)";
      default:
        return "rgba(75,200,75,0.9)";
    }
  }

  ColEstRep(Estatus){
    switch(Estatus){
      case "Desconectado":
        return "#666666";
      default:
        return "#34e916";
    }
  }

  ColEstVia(Estatus){
    switch(Estatus){
      case "Pendiente":
          return "#f25213";
      case "Nueva":
        return "#60cdd4";
      case "Aprobada":
          return "#32ad32";
      case "Pagada":
        return "#29a5dc";
      case "Aceptada":
        return "#29a5dc";
      case "Asignada":
        return "#29a5dc";
      case "Cerca":
        return "#32cf32";
      case "Iniciada":
        return "#f29b13";
      case "Finalizada":
        return "#8832cf";
      case "Calificado Profesional":
        return "#8832cf";
      case "Cancelada Profesional":
        return "#cf5132";
      case "Calificado Cliente":
        return "#8832cf";
      case "Cancelada Cliente":
        return "#cf5132";
      case "Terminado":
        return "#32ad32";
      case "En Devolucion":
        return "#f29b13";
      case "En Centro":
        return "#8832cf";
      case "Devuelto":
        return "#e99047";
    }
  }

  ColMon(Monto){
    if (Monto < 1){
      return "#f29b13";
    }else{
      return "#32cf32";
    }
  }

  ColEstTra(Estatus){
    switch(Estatus){
      case "Nueva":
        return "#60d49b";
      case "Aceptada":
        return "#29a5dc";
      case "Rechazada":
        return "#cf5132";
    }
  }

  TraDiaDis(MDS){
    let DDS = "";
    let DAS = "";
    for (let n = 0; n < MDS.length; n++){
      if (MDS[n] == "1"){
        DAS = "";
        switch (n){
          case 0:
            DAS = "Lun"
          break;
          case 1:
            DAS = "Mar"
          break;
          case 2:
            DAS = "Mie"
          break;
          case 3:
            DAS = "Jue"
          break;
          case 4:
            DAS = "Vie"
          break;
          case 5:
            DAS = "Sab"
          break;
          case 6:
            DAS = "Dom"
          break;
        }

        if (DDS == ""){
          DDS = DAS;
        }else{
          DDS = DDS + " / " + DAS;
        }
      }
    }
    return DDS;
  }

  DesDiaDis(MDS){
    let DDS = [];
    let DAS;
    for (let n = 0; n < MDS.length; n++){
      if (MDS[n] == "0"){
        DAS = null;
        switch (n){
          case 0:
            DAS = 1
          break;
          case 1:
            DAS = 2
          break;
          case 2:
            DAS = 3
          break;
          case 3:
            DAS = 4
          break;
          case 4:
            DAS = 5
          break;
          case 5:
            DAS = 6
          break;
          case 6:
            DAS = 0
          break;
        }

        DDS.push(DAS);
      }
    }
    return DDS;
  }

  DenFueGeo(GeoCerca,Ubicacion){
    let re = /\(/gi;
    let re2 = /\)/gi;
    GeoCerca = GeoCerca.replace(re,"");
    GeoCerca = GeoCerca.replace(re2,"");
    //console.log(GeoCerca);
    let ProPuntos = GeoCerca.split("!");
    //console.log(ProPuntos);

    //$GeoCerca = str_replace("(","",$GeoCerca);
    //$GeoCerca = str_replace(")","",$GeoCerca);
    //$ProPuntos = explode("!",$GeoCerca);

    //let Puntos = [{"Lat":"","Lon":"","Dis":0}]
    let Puntos = []
    for (let n2 = 0; n2 < ProPuntos.length; n2++) {
      let LatLonX = ProPuntos[n2].split(",");
      Puntos.push({"Lat":LatLonX[0],"Lon":LatLonX[1],"Dis":0})
    }
    //console.log(Puntos)

    /*
    for ($n2 = 0; $n2 < count($ProPuntos); $n2++) {
      $LatLonX = explode(",",$ProPuntos[$n2]);
      $Puntos[0][$n2] = trim($LatLonX[0]);
      $Puntos[1][$n2] = trim($LatLonX[1]);
    }
    */
    for (let n3 = 0; n3 < ProPuntos.length; n3++) {
      for (let n4 = 0; n4 < ProPuntos.length; n4++) {
        Puntos[n3].Dis = Puntos[n3].Dis + parseFloat(this.CalDisEntCooMts(Puntos[n3].Lat,Puntos[n3].Lon,Puntos[n4].Lat,Puntos[n4].Lon))
      }
    }
    /*
    for ($n3 = 0; $n3 < count($ProPuntos); $n3++){
      for ($n4 = 0; $n4 < count($ProPuntos); $n4++){
        $Puntos[2][$n3] = $Puntos[2][$n3] + CalDis2Pun($Puntos[0][$n3],$Puntos[1][$n3],$Puntos[0][$n4],$Puntos[1][$n4]);
      }
    }
    */
    let DisMay = 0;
    let DisMen = 0;
    for (let n5 = 0; n5 < ProPuntos.length; n5++) {
      if (DisMay < Puntos[n5].Dis){DisMay = Puntos[n5].Dis;}
      if (DisMen > Puntos[n5].Dis || DisMen == 0){DisMen = Puntos[n5].Dis;}
    }
    /*
    $DisMay = 0;
    $DisMen = 0;
    for ($n5 = 0; $n5 < count($ProPuntos); $n5++){
      if ($DisMay < $Puntos[2][$n5]){$DisMay = $Puntos[2][$n5];}
      if ($DisMen > $Puntos[2][$n5] || $DisMen == 0){$DisMen = $Puntos[2][$n5];}
    }
    */
      
    let MarErr = (DisMay - DisMen)/(ProPuntos.length -1)
    //$MarErr = ($DisMay - $DisMen)/(count($ProPuntos) -1);
    
    let LatLonUbi = Ubicacion.split(",");
    //$LatLonUbi = explode(",",$Ubicacion);
    let DisUbi = 0;
    for (let n6 = 0; n6 < ProPuntos.length; n6++) {
      DisUbi = DisUbi + parseFloat(this.CalDisEntCooMts(LatLonUbi[0],LatLonUbi[1],Puntos[n6].Lat,Puntos[n6].Lon))
    }
    /*
    for ($n6 = 0; $n6 < count($ProPuntos); $n6++){
      $DisUbi = $DisUbi + CalDis2Pun($LatLonUbi[0],$LatLonUbi[1],$Puntos[0][$n6],$Puntos[1][$n6]);
    }
    */

    if ((DisUbi + MarErr) > DisMay){
      return "Fuera";
    }else{
      return "Dentro";
    }
    /*
    if (($DisUbi + $MarErr) > $DisMay){
      return "Fuera";
    }else{
      return "Dentro";
    }
    */
  }

  CalDisEntCooMts(lat1,lon1,lat2,lon2){
    function rad(x){return x*Math.PI/180;}
    //rad = function(x) {return x*Math.PI/180;}
    //var R = 6378.137; //Radio de la tierra en km
    var R = 6378137; //Radio de la tierra en mts
    var dLat = rad( lat2 - lat1 );
    var dLong = rad( lon2 - lon1 );
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c;
    //return d.toFixed(3); //Retorna 3 decimales
    return d.toFixed(0); //Retorna 0 decimales
  }

  async UplFil(File) {
    let NEF = File.name.split(".");
    let d = new Date();
    let n = d.getTime();
    let NFN =  n +"."+ NEF[(NEF.length-1)];

    let file = File;
    let formData:FormData = new FormData();
    formData.append("file",file,NFN);
    let headers = new Headers();
    headers.append("Content-Type","multipart/form-data");
    headers.append("Accept","application/json");
    let options:any = ({headers:headers});
    let dataRes = await this.http.post(this.ApiUrl+"doc/usu/upload.php",formData,options).toPromise();
    //console.log(dataRes);
    return dataRes;
  }

  async SelFot(thiss){
    const actionSheet = await this.actionSheetController.create({
      header: "Seleccione la fuente de la imágen",
      cssClass: "my-custom-class",
      buttons: [
        {text: "Galería",
          //role: 'destructive',
          icon: "images-outline",
          handler: () => {this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY,thiss);}
        },
        {text: "Cámara",
          //role: 'destructive',
          icon: "camera-outline",
          handler: () => {this.takePicture(this.camera.PictureSourceType.CAMERA,thiss);}
        },
        {text: "Cancelar",
          icon: "close",
          role: "cancel",
          handler: () => {}
        }
      ]
    });
    await actionSheet.present();
  }

  takePicture(sourceType,thiss){
    var options = {
      quality: 75,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    
    this.camera.getPicture(options).then((imagePath) => {
      var correctPath;
      var currentName;
      if (sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
        this.copyFileToLocalDir(correctPath, currentName,thiss);
      }else{
        currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName,thiss);
      }
    }, (err) => {
      if (this.DebMod){
        this.EnvMsgSim("Error al seleccionar imagen",JSON.stringify(err));
      }else{
        this.EnvMsgToa("Error al seleccionar imagen");
      }
    });
  }

  copyFileToLocalDir(namePath, currentName,thiss) {
    this.MosLoaCon()

    var d = new Date();
    var n = d.getTime();
    var newFileName =  n + ".jpg";
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName,thiss).then(success => {
      this.lastImage = newFileName;
      this.uploadImage(thiss);
    },err => {
      if (this.DebMod){
        this.EnvMsgSim("Error almacenando el archivo",JSON.stringify(err));
      }else{
        this.EnvMsgToa("Error almacenando el archivo");
      }
      this.OcuLoaCon();
    });
  }

  uploadImage(thiss) {
    var url = encodeURI(this.ApiUrl+"img/"+thiss.DetFot.Carpeta+"/upload.php");
    var targetPath = this.pathForImage(this.lastImage,"",thiss.Carpeta);
    var filename = this.lastImage;

    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
   
    const fileTransfer: FileTransferObject = this.transfer.create();
    
    fileTransfer.upload(targetPath,url,options).then(data => {
      this.OcuLoaCon()
      this.EnvMsgToa("Imagen cargada correctamente!");

      switch (thiss.DetFot.Destino) {
        case "Registro":
          this.UsuReg.FotPer = filename;
        break;

        case "Perfil":
          this.UsuMat.FotPer = filename;
        break;

        case "CapPag":
          thiss.PedAct.CapPag = filename;
        break;

        case "Chat":
          thiss.NueFot = filename;
        break;

        case "Unidad":
          thiss.DatRec.Und.Foto = filename;
        break;
      }
      
    }, err => {
      this.OcuLoaCon()
      if (this.DebMod){
        this.EnvMsgSim("Error cargando la imagen",JSON.stringify(err));
      }else{
        this.EnvMsgToa("Error cargando la imagen");
      }
    });
  }

  pathForImage(img,Valor,Carpeta) {
    if (img === null){
      if (Valor == ""){
        return this.ApiUrl+Carpeta+"/fpd.png";
      }else{
        return this.ApiUrl+Carpeta+"/"+Valor;
      }
    }else{
      return cordova.file.dataDirectory+img;
    }
  }

  async EnvMsgSim(Tit,Men){
    const alert = await this.alertController.create({
      //cssClass: 'my-custom-class',
      header: Tit,
      //subHeader: 'Subtitle',
      message: Men,
      buttons: ["Aceptar"]
    });
    await alert.present();
  }

  async EnvMenCon(Tit,Men) {
    const alert = await this.alertController.create({
      //cssClass: 'my-custom-class',
      header: Tit,
      //subHeader: 'Subtitle',
      message: Men,
      buttons: [
        {
          text: "No",
          role: "cancel",
          cssClass: "secondary",
          handler: () => {
            return false
          }
        }, {
          text: "Si",
          handler: () => {
            return true
          }
        }
      ]
    });

    await alert.present();
  }

  async EnvMsgToa(Men) {
    const toast = await this.toastController.create({
      position: 'middle',
      message: Men,
      duration: 3000
    });
    toast.present();
  }

  NumFor(amount,decimals){
    let monori = amount;
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
        //return parseFloat('0').toFixed(decimals);
        return '0,00';
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
    regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');

    if (monori > 0){
      return amount_parts.join(',');
    }else{
      return "-"+amount_parts.join(',');
    }
  }

  ForHor(Hor){
    var MH = Hor.split(":");
    return MH[0]+":"+MH[1];
  }

  ForFec(Fec){
    var MF = Fec.split("-");
    return MF[2]+"-"+MF[1]+"-"+MF[0];
  }

  ForFecHor(FecHor){
    var MFH = FecHor.split(" ");
    var MF = MFH[0].split("-");
    return MF[2]+"-"+MF[1]+"-"+MF[0]+" "+MFH[1];
  }

  ForFecHorMed(FecHor){
    try{
      var MFH = FecHor.split(" ");
      var MF = MFH[0].split("-");
      var MH = MFH[1].split(":");
      return MF[2]+"-"+MF[1]+"-"+MF[0]+" "+MH[0]+":"+MH[1];
    }catch(error){
      return "";
    }
  }

  ForFecHorPeq(FecHor){
    try{
      var MFH = FecHor.split(" ");
      var MF = MFH[0].split("-");
      var MH = MFH[1].split(":");
      return MF[2]+"-"+MF[1]+"-"+MF[0].substr(-2)+" "+MH[0]+":"+MH[1];
    }catch(error){
      return "";
    }
  }

  async MosLoaCon(){
    return;
    if (this.LoaCon){return};
    this.LoaCon = await this.loadingController.create({
      //cssClass: 'my-custom-class',
      message: "Por favor espere ...",
      //duration: 2000
    });
    await this.LoaCon.present();
  }
  OcuLoaCon(){
    if (this.LoaCon){
      this.LoaCon.dismiss();
      this.LoaCon = null;
    }
  }

  GenCodAN(){
    let Caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    let Codigo = "";
    for (let i=0; i<6; i++) Codigo +=Caracteres.charAt(Math.floor(Math.random()*Caracteres.length)); 
    //console.log(Codigo)
    return Codigo;
  }

  GenCod4Dig(){
    var D1 = Math.random()*9;
    var D2 = Math.random()*9;
    var D3 = Math.random()*9;
    var D4 = Math.random()*9;
    D1 = Math.floor(D1);
    D2 = Math.floor(D2);
    D3 = Math.floor(D3);
    D4 = Math.floor(D4);

    console.log(D1+" "+D2+" "+D3+" "+D4);
    return D1+" "+D2+" "+D3+" "+D4;
  }

  CalSegEnt2Fec(FHPF,FHPA){
    var msecPerMinute = 1000 * 60;
    var msecPerHour = msecPerMinute * 60;
    var msecPerDay = msecPerHour * 24;
    var date;
    var dateMsec;
    var interval;
    var days;
    var hours;
    var minutes;
    var seconds;
    var MFH;
    var MF;
    var MH;

    //Fecha Mayor
    MFH = FHPF.split(" ");
    MF = MFH[0].split("-");
    MH = MFH[1].split(":");
    date = new Date(MF[0],MF[1],MF[2],MH[0],MH[1],MH[2]);
    dateMsec = date.getTime();
    
    //Fecha Menor
    MFH = FHPA.split(" ");
    MF = MFH[0].split("-");
    MH = MFH[1].split(":");
    date = new Date(MF[0],MF[1],MF[2],MH[0],MH[1],MH[2]);
    interval = dateMsec - date.getTime();
    
    //Calculos
    //days = Math.floor(interval / msecPerDay );
    //interval = interval - (days * msecPerDay );	
    //hours = Math.floor(interval / msecPerHour );
    //interval = interval - (hours * msecPerHour );	
    //minutes = Math.floor(interval / msecPerMinute );
    //interval = interval - (minutes * msecPerMinute );	
    seconds = Math.floor(interval / 1000 );
    
    //Devolver resultado
    //return days + "," + hours + "," + minutes + "," + seconds;
    return seconds;
  }

  CalDiaEnt2Fec(FHPF,FHPA){
    var msecPerMinute = 1000 * 60;
    var msecPerHour = msecPerMinute * 60;
    var msecPerDay = msecPerHour * 24;
    var date;
    var dateMsec;
    var interval;
    var days;
    var hours;
    var minutes;
    var seconds;
    var MFH;
    var MF;
    var MH;

    //Fecha Mayor
    MFH = FHPF.split(" ");
    MF = MFH[0].split("-");
    MH = MFH[1].split(":");
    date = new Date(MF[0],MF[1],MF[2],MH[0],MH[1],MH[2]);
    dateMsec = date.getTime();
    
    //Fecha Menor
    MFH = FHPA.split(" ");
    MF = MFH[0].split("-");
    MH = MFH[1].split(":");
    date = new Date(MF[0],MF[1],MF[2],MH[0],MH[1],MH[2]);
    interval = dateMsec - date.getTime();
    
    //Calculos
    days = Math.floor(interval / msecPerDay );
    //interval = interval - (days * msecPerDay );
    //hours = Math.floor(interval / msecPerHour);
    //interval = interval - (hours * msecPerHour );
    //minutes = Math.floor(interval / msecPerMinute );
    //interval = interval - (minutes * msecPerMinute );
    //seconds = Math.floor(interval / 1000 );
    
    //Devolver resultado
    //return days + "," + hours + "," + minutes + "," + seconds;
    return days;
  }

  ValCam(nombre,tipo,valor,requerido){
    if (requerido == "Si" && (valor == "" || valor == null)){
        this.EnvMsgSim(this.NomApp,"El campo "+nombre+" es requerido");
        return false;
    }else if (requerido != "Si" && (valor == "" || valor == null)){
        return true;
    }
    
    var filter=/^[A-Za-z0-9_.]*@[A-Za-z0-9_]+\.[A-Za-z0-9_.]+[A-za-z]$/;
    var filter2=/^[V|E|J][0-9]/;
    var filter3=/^[a-zA-Z ]*$/;
    var filter4=/^[0-9]*$/;
    var filter5=/^([0-2]\d):([0-5]\d):([0-5]\d)$/;
    var filter6=/\d{3}(.\d{2})(.\d{2})(.\d{2})/;
    var filter7=/^([0-9]){11}$/;
    var filter8=/^([0-9]){9,14}$/;
    var filter9=/^([0-9]){16}$/;
    var filter10=/^([0-9]{2})\/([0-9]{2})$/;
    var filter11=/^([0-9]){3}$/;
        
    switch (tipo){
        case "Texto":
          return true;
        
        case "Cedula":
        if (filter2.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser una Cedula válida");
            return false;
        }

        case "Telefono":
        if (filter8.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Telefono válido");
            return false;
        }
        
        case "Email":
        if (filter.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Email válido");
            return false;
        }

        case "SolLet":
        if (filter3.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser Solo Letras");
            return false;
        }
            
        case "SolNum":
        //if (!isNaN(valor)){
        if (filter4.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser Solo Numeros");
            return false;
        }

        case "Entero":
        if (isNaN(parseInt(valor))){
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Numero Entero válido");
            return false;
        }else{
            return true;
        }
        
        case "Decimal":
        if (isNaN(parseFloat(valor))){
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Numero Decimal válido");
            return false;
        }else{
            return true;
        }

        case "DecimalPositivo":
        if (isNaN(parseFloat(valor))){
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Numero Decimal válido");
            return false;
        }else{
          if (parseFloat(valor) > -1){
            return true;
          }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Numero Decimal Positivo");
            return false;
          }
        }

        case "Porcentaje":
        if (isNaN(parseFloat(valor))){
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Porcentaje válido");
            return false;
        }else{
            if (parseFloat(valor) > -1 && parseFloat(valor) < 101){
              return true;
            }else{
              this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un Porcentaje válido");
              return false;
            }
        }

        case "NumTarCre":
        if (filter9.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un numero de tarjeta válido");
            return false;
        }

        case "VenTarCre":
        if (filter10.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser una fecha de vencimiento válida MM/AA");
            return false;
        }

        case "CodTarCre":
        if (filter11.test(valor)){
            return true;
        }else{
            this.EnvMsgSim(this.NomApp,"El campo "+nombre+" debe ser un codigo de seguridad válido");
            return false;
        }

        default:
        return true;
    }
  }
}