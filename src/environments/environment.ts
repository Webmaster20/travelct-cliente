// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyD9kHIcZfgLkXjR319Wbb70YjAPAGeU-Xo",
  authDomain: "travelct-ee782.firebaseapp.com",
  projectId: "travelct-ee782",
  storageBucket: "travelct-ee782.appspot.com",
  messagingSenderId: "720187633013",
  appId: "1:720187633013:web:104f58600d28afe018773c",
  measurementId: "G-JLHSP4737Q"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
